/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50547
Source Host           : localhost:3306
Source Database       : armmycms

Target Server Type    : MYSQL
Target Server Version : 50547
File Encoding         : 65001

Date: 2017-12-29 17:43:13
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for ven_article
-- ----------------------------
DROP TABLE IF EXISTS `ven_article`;
CREATE TABLE `ven_article` (
  `id` bigint(18) unsigned NOT NULL AUTO_INCREMENT,
  `cid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '分类 ID',
  `title` varchar(100) NOT NULL DEFAULT '' COMMENT '标题',
  `faceimg` varchar(300) NOT NULL DEFAULT '' COMMENT '缩略图',
  `seo_title` varchar(100) NOT NULL DEFAULT '' COMMENT 'SEO 标题',
  `seo_keys` varchar(200) NOT NULL DEFAULT '' COMMENT 'SEO 关键词',
  `seo_desc` varchar(300) NOT NULL DEFAULT '' COMMENT 'SEO 描述',
  `author` varchar(30) NOT NULL DEFAULT '' COMMENT '作者',
  `source` varchar(100) NOT NULL DEFAULT '' COMMENT '来源',
  `resume` varchar(300) NOT NULL DEFAULT '' COMMENT '摘要',
  `content` text NOT NULL COMMENT '内容',
  `time_create` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `time_report` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '发布时间',
  `time_update` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `uid` int(6) unsigned NOT NULL DEFAULT '0' COMMENT '用户 ID',
  `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态，默认 0 待审核，1 已审核，-1 已删除',
  `click` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '阅读量',
  `file` varchar(300) DEFAULT NULL COMMENT '文件地址',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='文章表';

-- ----------------------------
-- Records of ven_article
-- ----------------------------
INSERT INTO `ven_article` VALUES ('1', '2', '行业新闻', '/uploads/article/201712\\df775aa3f5006838d191a51f89fe757c.jpg', '行业新闻行业新闻行业新闻行业新闻行业新闻', '行业新闻行业新闻行业新闻行业新闻', '行业新闻行业新闻行业新闻行业新闻行业新闻行业新闻', 'xiaocaihu.com', 'xiaocaihu.com', '行业新闻行业新闻行业新闻', '&lt;p style=&quot;margin-top: 0px; margin-bottom: 30px; padding: 0px; font-size: 18px; letter-spacing: 1px; line-height: 32px; color: rgb(77, 79, 83); font-family: &quot;Hiragino Sans GB&quot;, &quot;Microsoft Yahei&quot;, &quot;STHeiti Light&quot;, SimSun, Arial, sans-serif; white-space: normal;&quot;&gt;　据悉，&lt;span style=&quot;margin: 0px; padding: 0px;&quot;&gt;2018年宝马集团&lt;/span&gt;&lt;span style=&quot;margin: 0px; padding: 0px;&quot;&gt;将发布16款BMW和MINI新车及改款车型，除了上半年上市的宝马X2及年中上市的国产全新宝马X3外，宝马在中国推出的第6款新能源车——宝马5系插电式混合动力以及宝马新款i3/i3s也将于2018年推出。在大型豪华车市场，宝马将陆续推出&lt;/span&gt;宝马X7、宝马8系、宝马i8 Roadster等重磅车型。&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;http://db.auto.sina.com.cn/photo/130008793.html#130008793&quot; class=&quot; art_pic img&quot; target=&quot;_blank&quot; style=&quot;margin: 0px; padding: 0px; color: rgb(51, 51, 51); text-decoration: none; outline: none; display: block; text-align: center; position: relative;&quot;&gt;&lt;img src=&quot;/uploads/article/201712/c63af5ce61a3dcae224e64245041c0ff.jpeg&quot; alt=&quot;宝马X2实车官图发布 将在明年年初面世&quot; width=&quot;660&quot;/&gt;&lt;span class=&quot;img_t&quot; style=&quot;margin: 0px; padding: 5px 10px; background: rgba(0, 0, 0, 0.6); border-radius: 5px; position: absolute; color: rgb(255, 255, 255); top: 10px; right: 10px; border: 1px solid rgb(255, 255, 255);&quot;&gt;更多图片&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot;margin: 0px; padding: 0px 3px; color: rgb(223, 223, 223);&quot;&gt;|&lt;/span&gt;&lt;a href=&quot;http://db.auto.sina.com.cn/2462/peizhi/&quot; target=&quot;_blank&quot; style=&quot;margin: 0px; padding: 0px; color: rgb(153, 153, 153); text-decoration: none; outline: none;&quot;&gt;配置&lt;/a&gt;&lt;span style=&quot;margin: 0px; padding: 0px 3px; color: rgb(223, 223, 223);&quot;&gt;|&lt;/span&gt;&lt;a href=&quot;http://db.auto.sina.com.cn/photo/s2462.html&quot; target=&quot;_blank&quot; style=&quot;margin: 0px; padding: 0px; color: rgb(153, 153, 153); text-decoration: none; outline: none;&quot;&gt;图库&lt;/a&gt;&lt;span style=&quot;margin: 0px; padding: 0px 3px; color: rgb(223, 223, 223);&quot;&gt;|&lt;/span&gt;&amp;nbsp;&lt;a href=&quot;http://video.auto.sina.com.cn/2462/&quot; target=&quot;_blank&quot; style=&quot;margin: 0px; padding: 0px; color: rgb(153, 153, 153); text-decoration: none; outline: none;&quot;&gt;视频&lt;/a&gt;&lt;span style=&quot;margin: 0px; padding: 0px 3px; color: rgb(223, 223, 223);&quot;&gt;|&lt;/span&gt;&lt;a href=&quot;http://data.auto.sina.com.cn/car_comment/2462/&quot; target=&quot;_blank&quot; style=&quot;margin: 0px; padding: 0px; color: rgb(153, 153, 153); text-decoration: none; outline: none;&quot;&gt;口碑&lt;/a&gt;&lt;span style=&quot;margin: 0px; padding: 0px 3px; color: rgb(223, 223, 223);&quot;&gt;|&lt;/span&gt;&amp;nbsp;&lt;a href=&quot;http://dealer.auto.sina.com.cn/dealer/2_2462/&quot; target=&quot;_blank&quot; style=&quot;margin: 0px; padding: 0px; color: rgb(153, 153, 153); text-decoration: none; outline: none;&quot;&gt;经销商&lt;/a&gt;&lt;/p&gt;&lt;p&gt;&lt;span class=&quot;name&quot; style=&quot;margin: 0px; padding: 0px; font-weight: bold;&quot;&gt;&lt;a href=&quot;http://db.auto.sina.com.cn/2462/&quot; target=&quot;_blank&quot; style=&quot;margin: 0px; padding: 0px; color: rgb(51, 51, 51); text-decoration: none; outline: none;&quot;&gt;宝马X2&lt;/a&gt;&lt;/span&gt;&lt;span class=&quot;price-box&quot; style=&quot;margin: 0px; padding: 0px 80px 0px 5px; position: relative;&quot;&gt;&lt;span class=&quot;price&quot; style=&quot;margin: 0px 13px; padding: 0px; color: rgb(233, 71, 64);&quot;&gt;&lt;a href=&quot;http://db.auto.sina.com.cn/price/s2462.html&quot; target=&quot;_blank&quot; style=&quot;margin: 0px 13px; padding: 0px; color: rgb(233, 71, 64); text-decoration: none; outline: none;&quot;&gt;未上市&lt;/a&gt;&lt;/span&gt;&lt;a class=&quot;inquiryicon-btn J-auto-price-button&quot; style=&quot;margin: 0px; padding: 0px; color: rgb(255, 255, 255); outline: none; display: inline-block; width: 70px; height: 28px; line-height: 28px; background: rgb(233, 71, 64); text-align: center; border-radius: 3px; position: absolute; right: 0px; top: -5px;&quot;&gt;询价&lt;/a&gt;&lt;/span&gt;&lt;/p&gt;&lt;p style=&quot;margin-top: 0px; margin-bottom: 30px; padding: 0px; font-size: 18px; letter-spacing: 1px; line-height: 32px; color: rgb(77, 79, 83); font-family: &quot;Hiragino Sans GB&quot;, &quot;Microsoft Yahei&quot;, &quot;STHeiti Light&quot;, SimSun, Arial, sans-serif; white-space: normal;&quot;&gt;　　宝马X2基于UKL前驱平台打造，造型方面延续了此前概念车的设计，其前脸基本保持了宝马家族化的元素，但是像全新的双肾型进气格栅造型上已经发生了变化，车身侧面线条动感，尾部延续了家族式尾灯结构。内饰方面与同平台的宝马X1设计如出一辙，动力方面仍然搭载1.5T和2.0T发动机。&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;http://db.auto.sina.com.cn/photo/130000879.html#130000879&quot; class=&quot; art_pic img&quot; target=&quot;_blank&quot; style=&quot;margin: 0px; padding: 0px; color: rgb(51, 51, 51); text-decoration: none; outline: none; display: block; text-align: center; position: relative;&quot;&gt;&lt;img src=&quot;/uploads/article/201712/52ac6f837bc6fdbbb0e0514560e462cb.png&quot; alt=&quot;不变胜万变! 全新宝马X3将于2018年上市&quot; width=&quot;660&quot;/&gt;&lt;span class=&quot;img_t&quot; style=&quot;margin: 0px; padding: 5px 10px; background: rgba(0, 0, 0, 0.6); border-radius: 5px; position: absolute; color: rgb(255, 255, 255); top: 10px; right: 10px; border: 1px solid rgb(255, 255, 255);&quot;&gt;更多图片&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot;margin: 0px; padding: 0px 3px; color: rgb(223, 223, 223);&quot;&gt;|&lt;/span&gt;&lt;a href=&quot;http://db.auto.sina.com.cn/409/peizhi/&quot; target=&quot;_blank&quot; style=&quot;margin: 0px; padding: 0px; color: rgb(153, 153, 153); text-decoration: none; outline: none;&quot;&gt;配置&lt;/a&gt;&lt;span style=&quot;margin: 0px; padding: 0px 3px; color: rgb(223, 223, 223);&quot;&gt;|&lt;/span&gt;&lt;a href=&quot;http://db.auto.sina.com.cn/photo/s409.html&quot; target=&quot;_blank&quot; style=&quot;margin: 0px; padding: 0px; color: rgb(153, 153, 153); text-decoration: none; outline: none;&quot;&gt;图库&lt;/a&gt;&lt;span style=&quot;margin: 0px; padding: 0px 3px; color: rgb(223, 223, 223);&quot;&gt;|&lt;/span&gt;&amp;nbsp;&lt;a href=&quot;http://video.auto.sina.com.cn/409/&quot; target=&quot;_blank&quot; style=&quot;margin: 0px; padding: 0px; color: rgb(153, 153, 153); text-decoration: none; outline: none;&quot;&gt;视频&lt;/a&gt;&lt;span style=&quot;margin: 0px; padding: 0px 3px; color: rgb(223, 223, 223);&quot;&gt;|&lt;/span&gt;&lt;a href=&quot;http://data.auto.sina.com.cn/car_comment/409/&quot; target=&quot;_blank&quot; style=&quot;margin: 0px; padding: 0px; color: rgb(153, 153, 153); text-decoration: none; outline: none;&quot;&gt;口碑&lt;/a&gt;&lt;span style=&quot;margin: 0px; padding: 0px 3px; color: rgb(223, 223, 223);&quot;&gt;|&lt;/span&gt;&amp;nbsp;&lt;a href=&quot;http://dealer.auto.sina.com.cn/dealer/2_409/&quot; target=&quot;_blank&quot; style=&quot;margin: 0px; padding: 0px; color: rgb(153, 153, 153); text-decoration: none; outline: none;&quot;&gt;经销商&lt;/a&gt;&lt;/p&gt;&lt;p&gt;&lt;span class=&quot;name&quot; style=&quot;margin: 0px; padding: 0px; font-weight: bold;&quot;&gt;&lt;a href=&quot;http://db.auto.sina.com.cn/409/&quot; target=&quot;_blank&quot; style=&quot;margin: 0px; padding: 0px; color: rgb(51, 51, 51); text-decoration: none; outline: none;&quot;&gt;宝马X3&lt;/a&gt;&lt;/span&gt;&lt;span class=&quot;price-box&quot; style=&quot;margin: 0px; padding: 0px 80px 0px 5px; position: relative;&quot;&gt;&lt;span class=&quot;price&quot; style=&quot;margin: 0px 13px; padding: 0px; color: rgb(233, 71, 64);&quot;&gt;&lt;a href=&quot;http://db.auto.sina.com.cn/price/s409.html&quot; target=&quot;_blank&quot; style=&quot;margin: 0px 13px; padding: 0px; color: rgb(233, 71, 64); text-decoration: none; outline: none;&quot;&gt;42.10-75.00万元&lt;/a&gt;&lt;/span&gt;&lt;a class=&quot;inquiryicon-btn J-auto-price-button&quot; style=&quot;margin: 0px; padding: 0px; color: rgb(255, 255, 255); outline: none; display: inline-block; width: 70px; height: 28px; line-height: 28px; background: rgb(233, 71, 64); text-align: center; border-radius: 3px; position: absolute; right: 0px; top: -5px;&quot;&gt;询价&lt;/a&gt;&lt;/span&gt;&lt;/p&gt;&lt;p style=&quot;margin-top: 0px; margin-bottom: 30px; padding: 0px; font-size: 18px; letter-spacing: 1px; line-height: 32px; color: rgb(77, 79, 83); font-family: &quot;Hiragino Sans GB&quot;, &quot;Microsoft Yahei&quot;, &quot;STHeiti Light&quot;, SimSun, Arial, sans-serif; white-space: normal;&quot;&gt;　　国产全新宝马X3预计将延续海外版全新宝马X3的设计，并不会推出加长版车型。新车采用了全新的家族式外观设计，整体风格更加时尚。&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;http://db.auto.sina.com.cn/photo/129958299.html#129958299&quot; class=&quot; art_pic img&quot; target=&quot;_blank&quot; style=&quot;margin: 0px; padding: 0px; color: rgb(51, 51, 51); text-decoration: none; outline: none; display: block; text-align: center; position: relative;&quot;&gt;&lt;img src=&quot;/uploads/article/201712/8a8aa9b1cdee36243356cb3f43d015a1.jpeg&quot; alt=&quot;法兰克福车展 宝马i Vision Dynamics首发亮相&quot; width=&quot;660&quot;/&gt;&lt;span class=&quot;img_t&quot; style=&quot;margin: 0px; padding: 5px 10px; background: rgba(0, 0, 0, 0.6); border-radius: 5px; position: absolute; color: rgb(255, 255, 255); top: 10px; right: 10px; border: 1px solid rgb(255, 255, 255);&quot;&gt;更多图片&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot;margin: 0px; padding: 0px 3px; color: rgb(223, 223, 223);&quot;&gt;|&lt;/span&gt;&lt;a href=&quot;http://db.auto.sina.com.cn/2918/peizhi/&quot; target=&quot;_blank&quot; style=&quot;margin: 0px; padding: 0px; color: rgb(153, 153, 153); text-decoration: none; outline: none;&quot;&gt;配置&lt;/a&gt;&lt;span style=&quot;margin: 0px; padding: 0px 3px; color: rgb(223, 223, 223);&quot;&gt;|&lt;/span&gt;&lt;a href=&quot;http://db.auto.sina.com.cn/photo/s2918.html&quot; target=&quot;_blank&quot; style=&quot;margin: 0px; padding: 0px; color: rgb(153, 153, 153); text-decoration: none; outline: none;&quot;&gt;图库&lt;/a&gt;&lt;span style=&quot;margin: 0px; padding: 0px 3px; color: rgb(223, 223, 223);&quot;&gt;|&lt;/span&gt;&amp;nbsp;&lt;a href=&quot;http://video.auto.sina.com.cn/2918/&quot; target=&quot;_blank&quot; style=&quot;margin: 0px; padding: 0px; color: rgb(153, 153, 153); text-decoration: none; outline: none;&quot;&gt;视频&lt;/a&gt;&lt;span style=&quot;margin: 0px; padding: 0px 3px; color: rgb(223, 223, 223);&quot;&gt;|&lt;/span&gt;&lt;a href=&quot;http://data.auto.sina.com.cn/car_comment/2918/&quot; target=&quot;_blank&quot; style=&quot;margin: 0px; padding: 0px; color: rgb(153, 153, 153); text-decoration: none; outline: none;&quot;&gt;口碑&lt;/a&gt;&lt;span style=&quot;margin: 0px; padding: 0px 3px; color: rgb(223, 223, 223);&quot;&gt;|&lt;/span&gt;&amp;nbsp;&lt;a href=&quot;http://dealer.auto.sina.com.cn/dealer/2_2918/&quot; target=&quot;_blank&quot; style=&quot;margin: 0px; padding: 0px; color: rgb(153, 153, 153); text-decoration: none; outline: none;&quot;&gt;经销商&lt;/a&gt;&lt;/p&gt;&lt;p&gt;&lt;span class=&quot;name&quot; style=&quot;margin: 0px; padding: 0px; font-weight: bold;&quot;&gt;&lt;a href=&quot;http://db.auto.sina.com.cn/2918/&quot; target=&quot;_blank&quot; style=&quot;margin: 0px; padding: 0px; color: rgb(51, 51, 51); text-decoration: none; outline: none;&quot;&gt;宝马i Vision Dynamics&lt;/a&gt;&lt;/span&gt;&lt;span class=&quot;price-box&quot; style=&quot;margin: 0px; padding: 0px 80px 0px 5px; position: relative;&quot;&gt;&lt;span class=&quot;price&quot; style=&quot;margin: 0px 13px; padding: 0px; color: rgb(233, 71, 64);&quot;&gt;&lt;a href=&quot;http://db.auto.sina.com.cn/price/s2918.html&quot; target=&quot;_blank&quot; style=&quot;margin: 0px 13px; padding: 0px; color: rgb(233, 71, 64); text-decoration: none; outline: none;&quot;&gt;未上市&lt;/a&gt;&lt;/span&gt;&lt;a class=&quot;inquiryicon-btn J-auto-price-button&quot; style=&quot;margin: 0px; padding: 0px; color: rgb(255, 255, 255); outline: none; display: inline-block; width: 70px; height: 28px; line-height: 28px; background: rgb(233, 71, 64); text-align: center; border-radius: 3px; position: absolute; right: 0px; top: -5px;&quot;&gt;询价&lt;/a&gt;&lt;/span&gt;&lt;/p&gt;&lt;p style=&quot;margin-top: 0px; margin-bottom: 30px; padding: 0px; font-size: 18px; letter-spacing: 1px; line-height: 32px; color: rgb(77, 79, 83); font-family: &quot;Hiragino Sans GB&quot;, &quot;Microsoft Yahei&quot;, &quot;STHeiti Light&quot;, SimSun, Arial, sans-serif; white-space: normal;&quot;&gt;　　新能源方面，2025年之前，宝马将发布25款新能源车型，其中12 款将是纯电动车。未来宝马的电动车型将全部归于子品牌BMW i旗下。据悉，宝马已经成功注册了从BMW i1到i9，以及BMW iX1到iX9的全部产品名称。在2017法兰克福车展首发的MINI Electric概念车将在2019年正式量产；2020年全新一代宝马X3将推出纯电动版本；2021年，宝马将推出iNEXT概念车的量产版，并搭载宝马第五代纯电动系统，并实现高度自动驾驶技术的量产化。&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;http://db.auto.sina.com.cn/photo/129936482.html#129936482&quot; class=&quot; art_pic img&quot; target=&quot;_blank&quot; style=&quot;margin: 0px; padding: 0px; color: rgb(51, 51, 51); text-decoration: none; outline: none; display: block; text-align: center; position: relative;&quot;&gt;&lt;img src=&quot;/uploads/article/201712/3b69cac03dcd65c166d4741c7a9225d1.jpeg&quot; alt=&quot;2018款宝马7系740Li 3.0T自动尊享型卓越套装xDrive&quot; width=&quot;660&quot;/&gt;&lt;span class=&quot;img_t&quot; style=&quot;margin: 0px; padding: 5px 10px; background: rgba(0, 0, 0, 0.6); border-radius: 5px; position: absolute; color: rgb(255, 255, 255); top: 10px; right: 10px; border: 1px solid rgb(255, 255, 255);&quot;&gt;更多图片&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot;margin: 0px; padding: 0px 3px; color: rgb(223, 223, 223);&quot;&gt;|&lt;/span&gt;&lt;a href=&quot;http://db.auto.sina.com.cn/335/peizhi/&quot; target=&quot;_blank&quot; style=&quot;margin: 0px; padding: 0px; color: rgb(153, 153, 153); text-decoration: none; outline: none;&quot;&gt;配置&lt;/a&gt;&lt;span style=&quot;margin: 0px; padding: 0px 3px; color: rgb(223, 223, 223);&quot;&gt;|&lt;/span&gt;&lt;a href=&quot;http://db.auto.sina.com.cn/photo/s335.html&quot; target=&quot;_blank&quot; style=&quot;margin: 0px; padding: 0px; color: rgb(153, 153, 153); text-decoration: none; outline: none;&quot;&gt;图库&lt;/a&gt;&lt;span style=&quot;margin: 0px; padding: 0px 3px; color: rgb(223, 223, 223);&quot;&gt;|&lt;/span&gt;&amp;nbsp;&lt;a href=&quot;http://video.auto.sina.com.cn/335/&quot; target=&quot;_blank&quot; style=&quot;margin: 0px; padding: 0px; color: rgb(153, 153, 153); text-decoration: none; outline: none;&quot;&gt;视频&lt;/a&gt;&lt;span style=&quot;margin: 0px; padding: 0px 3px; color: rgb(223, 223, 223);&quot;&gt;|&lt;/span&gt;&lt;a href=&quot;http://data.auto.sina.com.cn/car_comment/335/&quot; target=&quot;_blank&quot; style=&quot;margin: 0px; padding: 0px; color: rgb(153, 153, 153); text-decoration: none; outline: none;&quot;&gt;口碑&lt;/a&gt;&lt;span style=&quot;margin: 0px; padding: 0px 3px; color: rgb(223, 223, 223);&quot;&gt;|&lt;/span&gt;&amp;nbsp;&lt;a href=&quot;http://dealer.auto.sina.com.cn/dealer/2_335/&quot; target=&quot;_blank&quot; style=&quot;margin: 0px; padding: 0px; color: rgb(153, 153, 153); text-decoration: none; outline: none;&quot;&gt;经销商&lt;/a&gt;&lt;/p&gt;&lt;p&gt;&lt;span class=&quot;name&quot; style=&quot;margin: 0px; padding: 0px; font-weight: bold;&quot;&gt;&lt;a href=&quot;http://db.auto.sina.com.cn/335/&quot; target=&quot;_blank&quot; style=&quot;margin: 0px; padding: 0px; color: rgb(51, 51, 51); text-decoration: none; outline: none;&quot;&gt;宝马7系&lt;/a&gt;&lt;/span&gt;&lt;span class=&quot;price-box&quot; style=&quot;margin: 0px; padding: 0px 80px 0px 5px; position: relative;&quot;&gt;&lt;span class=&quot;price&quot; style=&quot;margin: 0px 13px; padding: 0px; color: rgb(233, 71, 64);&quot;&gt;&lt;a href=&quot;http://db.auto.sina.com.cn/price/s335.html&quot; target=&quot;_blank&quot; style=&quot;margin: 0px 13px; padding: 0px; color: rgb(233, 71, 64); text-decoration: none; outline: none;&quot;&gt;66.00-265.80万元&lt;/a&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;', '1513742230', '1513699200', '1513763541', '0', '1', '0', '/uploads/wordfile/201712\\131195df9212ae49d198fd4c598d1204.rar');

-- ----------------------------
-- Table structure for ven_authaccess
-- ----------------------------
DROP TABLE IF EXISTS `ven_authaccess`;
CREATE TABLE `ven_authaccess` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '角色 ID',
  `rule_name` varchar(255) NOT NULL DEFAULT '' COMMENT '规则唯一英文标识,全小写',
  PRIMARY KEY (`id`),
  KEY `role_id` (`role_id`),
  KEY `rule_name` (`rule_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='后台权限授权表';

-- ----------------------------
-- Records of ven_authaccess
-- ----------------------------

-- ----------------------------
-- Table structure for ven_category
-- ----------------------------
DROP TABLE IF EXISTS `ven_category`;
CREATE TABLE `ven_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '上级分类 ID',
  `path` varchar(100) NOT NULL DEFAULT '' COMMENT '分类层次',
  `name` varchar(100) NOT NULL DEFAULT '' COMMENT '分类名称',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '状态，默认 0 隐藏 1 显示',
  `seo_title` varchar(100) NOT NULL DEFAULT '' COMMENT 'SEO 标题',
  `seo_keys` varchar(200) NOT NULL DEFAULT '' COMMENT 'SEO 关键词',
  `seo_desc` varchar(300) NOT NULL DEFAULT '' COMMENT 'SEO 描述',
  `price` decimal(10,0) NOT NULL DEFAULT '0' COMMENT '价格',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8 COMMENT='文章分类表';

-- ----------------------------
-- Records of ven_category
-- ----------------------------
INSERT INTO `ven_category` VALUES ('1', '0', '0-1', '中国乘用车信息网', '0', '中国乘用车信息网', '中国乘用车信息网', '中国乘用车信息网', '10');
INSERT INTO `ven_category` VALUES ('2', '1', '0-1-2', '行业快讯', '1', '中国乘用车信息网行业快讯', '中国乘用车信息网行业快讯', '中国乘用车信息网行业快讯', '10');
INSERT INTO `ven_category` VALUES ('3', '1', '0-1-3', '热点分析', '0', '', '道学', '道学道学', '10');
INSERT INTO `ven_category` VALUES ('4', '1', '0-1-4', '数据透视', '0', '数据透视', '数据透视', '数据透视', '10');
INSERT INTO `ven_category` VALUES ('22', '4', '0-1-4-22', '轿车', '0', '', '', '', '10');
INSERT INTO `ven_category` VALUES ('23', '4', '0-1-4-23', 'SUV', '0', '', '', '', '10');
INSERT INTO `ven_category` VALUES ('24', '4', '0-1-4-24', 'MPV', '0', '', '', '', '10');
INSERT INTO `ven_category` VALUES ('25', '4', '0-1-4-25', '新能源', '0', '', '', '', '10');
INSERT INTO `ven_category` VALUES ('27', '4', '0-1-4-27', '进出口', '0', '', '', '', '10');
INSERT INTO `ven_category` VALUES ('28', '1', '0-1-28', '专题研究', '1', '', '', '', '10999');
INSERT INTO `ven_category` VALUES ('29', '0', '0-29', '中国商用车信息网', '1', '中国商用车信息网', '中国商用车信息网', '中国商用车信息网', '10');
INSERT INTO `ven_category` VALUES ('30', '29', '0-29-30', '行业快讯', '1', '', '', '', '0');
INSERT INTO `ven_category` VALUES ('31', '29', '0-29-31', '热点分析', '1', '', '', '', '0');
INSERT INTO `ven_category` VALUES ('33', '29', '0-29-33', '数据透视', '1', '', '', '', '0');
INSERT INTO `ven_category` VALUES ('34', '29', '0-29-34', '专题研究', '1', '', '', '', '0');
INSERT INTO `ven_category` VALUES ('35', '33', '0-29-33-35', '微客', '0', '', '', '', '0');
INSERT INTO `ven_category` VALUES ('36', '33', '0-29-33-36', '轻客', '0', '', '', '', '0');
INSERT INTO `ven_category` VALUES ('37', '33', '0-29-33-37', '大中客', '0', '', '', '', '0');
INSERT INTO `ven_category` VALUES ('38', '33', '0-29-33-38', '微卡', '0', '', '', '', '0');
INSERT INTO `ven_category` VALUES ('39', '33', '0-29-33-39', '皮卡', '0', '', '', '', '0');
INSERT INTO `ven_category` VALUES ('40', '33', '0-29-33-40', '轻卡', '0', '', '', '', '0');
INSERT INTO `ven_category` VALUES ('41', '33', '0-29-33-41', '中卡', '0', '', '', '', '0');
INSERT INTO `ven_category` VALUES ('43', '33', '0-29-33-43', '重卡', '0', '', '', '', '0');
INSERT INTO `ven_category` VALUES ('44', '33', '0-29-33-44', '牵引', '0', '', '', '', '0');
INSERT INTO `ven_category` VALUES ('45', '33', '0-29-33-45', '自卸', '0', '', '', '', '0');
INSERT INTO `ven_category` VALUES ('46', '33', '0-29-33-46', '新能源', '0', '', '', '', '0');
INSERT INTO `ven_category` VALUES ('47', '33', '0-29-33-47', '进出口', '0', '', '', '', '0');

-- ----------------------------
-- Table structure for ven_menu
-- ----------------------------
DROP TABLE IF EXISTS `ven_menu`;
CREATE TABLE `ven_menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '上级菜单 ID',
  `topid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '所属顶级 ID',
  `module` varchar(30) NOT NULL DEFAULT '' COMMENT '模块名称',
  `control` varchar(30) NOT NULL DEFAULT '' COMMENT '控制器名称',
  `actions` varchar(30) NOT NULL DEFAULT '' COMMENT '方法名称',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '状态，默认 0 隐藏 1 显示',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '菜单名称',
  `icon` varchar(50) DEFAULT '' COMMENT '菜单图标',
  `path` varchar(100) NOT NULL DEFAULT '' COMMENT '层级关系',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8 COMMENT='后台菜单表';

-- ----------------------------
-- Records of ven_menu
-- ----------------------------
INSERT INTO `ven_menu` VALUES ('1', '0', '0', 'admin', 'common', 'default', '1', '公共项目', '', '0-1');
INSERT INTO `ven_menu` VALUES ('2', '1', '1', 'admin', 'setting', 'recache', '1', '刷新缓存', '', '0-1-2');
INSERT INTO `ven_menu` VALUES ('3', '1', '1', 'admin', 'myinfo', 'repasswd', '1', '修改密码', '', '0-1-3');
INSERT INTO `ven_menu` VALUES ('4', '0', '0', 'admin', 'manager', 'default', '1', '权限管理', '', '0-4');
INSERT INTO `ven_menu` VALUES ('5', '4', '4', 'admin', 'menu', 'index', '1', '菜单管理', '', '0-4-5');
INSERT INTO `ven_menu` VALUES ('6', '5', '4', 'admin', 'menu', 'add', '0', '新增菜单', '', '0-4-5-6');
INSERT INTO `ven_menu` VALUES ('7', '6', '4', 'admin', 'menu', 'addsave', '0', '保存新增菜单', '', '0-4-5-6-7');
INSERT INTO `ven_menu` VALUES ('8', '5', '4', 'admin', 'menu', 'edit', '0', '编辑菜单', '', '0-4-5-8');
INSERT INTO `ven_menu` VALUES ('9', '8', '4', 'admin', 'menu', 'editsave', '0', '保存编辑菜单', '', '0-4-5-8-9');
INSERT INTO `ven_menu` VALUES ('10', '5', '4', 'admin', 'menu', 'deletes', '0', '删除菜单', '', '0-4-5-10');
INSERT INTO `ven_menu` VALUES ('11', '4', '4', 'admin', 'rbac', 'index', '1', '角色管理', '', '0-4-11');
INSERT INTO `ven_menu` VALUES ('12', '11', '4', 'admin', 'rbac', 'add', '0', '新增角色', '', '0-4-11-12');
INSERT INTO `ven_menu` VALUES ('13', '12', '4', 'admin', 'rbac', 'addsave', '0', '保存新增角色', '', '0-4-11-12-13');
INSERT INTO `ven_menu` VALUES ('14', '11', '4', 'admin', 'rbac', 'edit', '0', '编辑角色', '', '0-4-11-14');
INSERT INTO `ven_menu` VALUES ('15', '14', '4', 'admin', 'rbac', 'editsave', '0', '保存编辑角色', '', '0-4-11-14-15');
INSERT INTO `ven_menu` VALUES ('16', '11', '4', 'admin', 'rbac', 'deletes', '0', '删除角色', '', '0-4-11-16');
INSERT INTO `ven_menu` VALUES ('17', '11', '4', 'admin', 'rbac', 'authorize', '0', '权限设置', '', '0-4-11-17');
INSERT INTO `ven_menu` VALUES ('18', '17', '4', 'admin', 'rbac', 'authorizesave', '0', '保存权限设置', '', '0-4-11-17-18');
INSERT INTO `ven_menu` VALUES ('19', '4', '4', 'admin', 'users', 'index', '1', '用户管理', '', '0-4-19');
INSERT INTO `ven_menu` VALUES ('20', '19', '4', 'admin', 'users', 'add', '0', '新增用户', '', '0-4-19-20');
INSERT INTO `ven_menu` VALUES ('21', '20', '4', 'admin', 'users', 'addsave', '0', '保存新增用户', '', '0-4-19-20-21');
INSERT INTO `ven_menu` VALUES ('22', '19', '4', 'admin', 'users', 'edit', '0', '编辑用户', '', '0-4-19-22');
INSERT INTO `ven_menu` VALUES ('23', '22', '4', 'admin', 'users', 'editsave', '0', '保存编辑用户', '', '0-4-19-22-23');
INSERT INTO `ven_menu` VALUES ('24', '19', '4', 'admin', 'users', 'deletes', '0', '删除用户', '', '0-4-19-24');
INSERT INTO `ven_menu` VALUES ('25', '0', '0', 'admin', 'setting', 'default', '1', '网站设置', '', '0-25');
INSERT INTO `ven_menu` VALUES ('26', '25', '25', 'admin', 'setting', 'siteinfo', '1', '网站信息', '', '0-25-26');
INSERT INTO `ven_menu` VALUES ('27', '26', '25', 'admin', 'setting', 'siteinfosave', '0', '保存网站信息', '', '0-25-26-27');
INSERT INTO `ven_menu` VALUES ('28', '25', '25', 'admin', 'setting', 'imgset', '1', '图片设置', '', '0-25-28');
INSERT INTO `ven_menu` VALUES ('29', '28', '25', 'admin', 'setting', 'imgsetsave', '0', '保存图片设置', '', '0-25-28-29');
INSERT INTO `ven_menu` VALUES ('30', '0', '0', 'admin', 'content', 'default', '1', '资料管理', '', '0-30');
INSERT INTO `ven_menu` VALUES ('31', '30', '30', 'admin', 'category', 'index', '1', '分类管理', '', '0-30-31');
INSERT INTO `ven_menu` VALUES ('32', '31', '30', 'admin', 'category', 'add', '0', '新增分类', '', '0-30-31-32');
INSERT INTO `ven_menu` VALUES ('33', '32', '30', 'admin', 'category', 'addsave', '0', '保存新增分类', '', '0-30-31-32-33');
INSERT INTO `ven_menu` VALUES ('34', '31', '30', 'admin', 'category', 'edit', '0', '编辑分类', '', '0-30-31-34');
INSERT INTO `ven_menu` VALUES ('35', '34', '30', 'admin', 'category', 'editsave', '0', '保存编辑分类', '', '0-30-31-34-35');
INSERT INTO `ven_menu` VALUES ('36', '31', '30', 'admin', 'category', 'deletes', '0', '删除分类', '', '0-30-31-36');
INSERT INTO `ven_menu` VALUES ('37', '30', '30', 'admin', 'article', 'index', '1', '资料管理', '', '0-30-37');
INSERT INTO `ven_menu` VALUES ('38', '37', '30', 'admin', 'article', 'add', '0', '新增文章', '', '0-30-37-38');
INSERT INTO `ven_menu` VALUES ('39', '38', '30', 'admin', 'article', 'addsave', '0', '保存新增文章', '', '0-30-37-38-39');
INSERT INTO `ven_menu` VALUES ('40', '37', '30', 'admin', 'article', 'edit', '0', '编辑文章', '', '0-30-37-40');
INSERT INTO `ven_menu` VALUES ('41', '40', '30', 'admin', 'article', 'editsave', '0', '保存编辑文章', '', '0-30-37-40-41');
INSERT INTO `ven_menu` VALUES ('42', '37', '30', 'admin', 'article', 'deletes', '0', '删除文章', '', '0-30-37-42');
INSERT INTO `ven_menu` VALUES ('43', '37', '30', 'admin', 'article', 'editstatus', '0', '编辑文章状态', '', '0-30-37-43');
INSERT INTO `ven_menu` VALUES ('44', '37', '30', 'admin', 'article', 'upimage', '0', '上传文章缩略图', '', '0-30-37-44');
INSERT INTO `ven_menu` VALUES ('45', '37', '30', 'admin', 'ueditor', 'doupload', '0', '上传文章内容图片', '', '0-30-37-45');
INSERT INTO `ven_menu` VALUES ('46', '0', '0', 'admin', 'tools', 'default', '1', '扩展工具', '', '0-46');
INSERT INTO `ven_menu` VALUES ('47', '46', '46', 'admin', 'slide', 'default', '1', '幻灯片', '', '0-46-47');
INSERT INTO `ven_menu` VALUES ('48', '47', '46', 'admin', 'slideseat', 'index', '1', '幻灯片分类', '', '0-46-47-48');
INSERT INTO `ven_menu` VALUES ('49', '48', '46', 'admin', 'slideseat', 'add', '0', '新增幻灯片分类', '', '0-46-47-48-49');
INSERT INTO `ven_menu` VALUES ('50', '49', '46', 'admin', 'slideseat', 'addsave', '0', '保存新增幻灯片分类', '', '0-46-47-48-49-50');
INSERT INTO `ven_menu` VALUES ('51', '48', '46', 'admin', 'slideseat', 'edit', '0', '编辑幻灯片分类', '', '0-46-47-48-51');
INSERT INTO `ven_menu` VALUES ('52', '51', '46', 'admin', 'slideseat', 'editsave', '0', '保存编辑幻灯片分类', '', '0-46-47-48-51-52');
INSERT INTO `ven_menu` VALUES ('53', '48', '46', 'admin', 'slideseat', 'deletes', '0', '删除幻灯片分类', '', '0-46-47-48-53');
INSERT INTO `ven_menu` VALUES ('54', '47', '46', 'admin', 'slide', 'index', '1', '幻灯片管理', '', '0-46-47-54');
INSERT INTO `ven_menu` VALUES ('55', '54', '46', 'admin', 'slide', 'add', '0', '新增幻灯片', '', '0-46-47-54-55');
INSERT INTO `ven_menu` VALUES ('56', '55', '46', 'admin', 'slide', 'addsave', '0', '保存新增幻灯片', '', '0-46-47-54-55-56');
INSERT INTO `ven_menu` VALUES ('57', '54', '46', 'admin', 'slide', 'edit', '0', '编辑幻灯片', '', '0-46-47-54-57');
INSERT INTO `ven_menu` VALUES ('58', '57', '46', 'admin', 'slide', 'editsave', '0', '保存编辑幻灯片', '', '0-46-47-54-57-58');
INSERT INTO `ven_menu` VALUES ('59', '54', '46', 'admin', 'slide', 'deletes', '0', '删除幻灯片', '', '0-46-47-54-59');
INSERT INTO `ven_menu` VALUES ('60', '54', '46', 'admin', 'slide', 'editstatus', '0', '编辑幻灯片状态', '', '0-46-47-54-60');
INSERT INTO `ven_menu` VALUES ('61', '54', '46', 'admin', 'slide', 'upimage', '0', '上传幻灯片图片', '', '0-46-47-54-61');
INSERT INTO `ven_menu` VALUES ('62', '0', '0', 'admin', 'member', 'default', '1', '用户管理', '', '0-62');
INSERT INTO `ven_menu` VALUES ('63', '62', '62', 'admin', 'member', 'index', '1', '用户列表', '', '0-62-63');
INSERT INTO `ven_menu` VALUES ('64', '0', '0', 'admin', 'weixin', 'default', '1', '微信管理', '', '0-64');

-- ----------------------------
-- Table structure for ven_options
-- ----------------------------
DROP TABLE IF EXISTS `ven_options`;
CREATE TABLE `ven_options` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL DEFAULT '' COMMENT '参数标题',
  `content` varchar(5000) NOT NULL DEFAULT '' COMMENT '参数内容，json 格式',
  `remark` varchar(200) NOT NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='基础参数表';

-- ----------------------------
-- Records of ven_options
-- ----------------------------
INSERT INTO `ven_options` VALUES ('1', 'siteinfo', '', '网站基本信息');

-- ----------------------------
-- Table structure for ven_role
-- ----------------------------
DROP TABLE IF EXISTS `ven_role`;
CREATE TABLE `ven_role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL DEFAULT '' COMMENT '角色名称',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '状态，默认 0 禁用 1 启用',
  `remark` varchar(255) NOT NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='后台角色表';

-- ----------------------------
-- Records of ven_role
-- ----------------------------
INSERT INTO `ven_role` VALUES ('1', '超级管理员', '1', '拥有网站最高管理员权限！');
INSERT INTO `ven_role` VALUES ('2', '编辑', '1', '');
INSERT INTO `ven_role` VALUES ('3', '合作商', '1', '合作合作');

-- ----------------------------
-- Table structure for ven_roleuser
-- ----------------------------
DROP TABLE IF EXISTS `ven_roleuser`;
CREATE TABLE `ven_roleuser` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '角色 ID',
  `uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '管理员 ID',
  PRIMARY KEY (`id`),
  KEY `role_id` (`role_id`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='后台用户角色对应表';

-- ----------------------------
-- Records of ven_roleuser
-- ----------------------------
INSERT INTO `ven_roleuser` VALUES ('6', '2', '501');

-- ----------------------------
-- Table structure for ven_slide
-- ----------------------------
DROP TABLE IF EXISTS `ven_slide`;
CREATE TABLE `ven_slide` (
  `id` bigint(18) unsigned NOT NULL AUTO_INCREMENT,
  `cid` int(6) unsigned NOT NULL DEFAULT '0' COMMENT '幻灯片分类 ID',
  `title` varchar(100) NOT NULL DEFAULT '' COMMENT '标题',
  `faceimg` varchar(300) NOT NULL DEFAULT '' COMMENT '图片',
  `url` varchar(300) NOT NULL DEFAULT '' COMMENT '幻灯片链接',
  `time_create` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `time_update` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态，默认 0 隐藏，1 显示',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='幻灯片内容表';

-- ----------------------------
-- Records of ven_slide
-- ----------------------------
INSERT INTO `ven_slide` VALUES ('1', '2', '金条大甩卖', '/uploads/article/201709/1505100994210.jpg', 'http://www.hao123.com', '1505100609', '1506324856', '0');

-- ----------------------------
-- Table structure for ven_slideseat
-- ----------------------------
DROP TABLE IF EXISTS `ven_slideseat`;
CREATE TABLE `ven_slideseat` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL DEFAULT '' COMMENT '分类名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='幻灯片分类表';

-- ----------------------------
-- Records of ven_slideseat
-- ----------------------------
INSERT INTO `ven_slideseat` VALUES ('1', '首页');
INSERT INTO `ven_slideseat` VALUES ('2', '详情');

-- ----------------------------
-- Table structure for ven_sms_log
-- ----------------------------
DROP TABLE IF EXISTS `ven_sms_log`;
CREATE TABLE `ven_sms_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '表id',
  `mobile` varchar(11) CHARACTER SET latin1 DEFAULT '' COMMENT '手机号',
  `session_id` varchar(128) CHARACTER SET latin1 DEFAULT '' COMMENT 'session_id',
  `add_time` int(11) DEFAULT '0' COMMENT '发送时间',
  `code` varchar(10) CHARACTER SET latin1 DEFAULT '' COMMENT '验证码',
  `status` int(1) NOT NULL DEFAULT '0' COMMENT '发送状态,1:成功,0:失败',
  `msg` varchar(255) CHARACTER SET latin1 DEFAULT NULL COMMENT '短信内容',
  `scene` int(1) DEFAULT '0' COMMENT '发送场景,1:用户注册,2:找回密码,3:身份验证',
  `error_msg` text CHARACTER SET latin1 NOT NULL COMMENT '发送短信异常内容',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ven_sms_log
-- ----------------------------

-- ----------------------------
-- Table structure for ven_sms_template
-- ----------------------------
DROP TABLE IF EXISTS `ven_sms_template`;
CREATE TABLE `ven_sms_template` (
  `tpl_id` mediumint(8) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `sms_sign` varchar(50) NOT NULL COMMENT '短信签名',
  `sms_tpl_code` varchar(100) NOT NULL COMMENT '短信模板ID',
  `tpl_content` varchar(512) NOT NULL COMMENT '发送短信内容',
  `send_scene` varchar(100) NOT NULL COMMENT '短信发送场景',
  `add_time` int(11) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`tpl_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ven_sms_template
-- ----------------------------

-- ----------------------------
-- Table structure for ven_user
-- ----------------------------
DROP TABLE IF EXISTS `ven_user`;
CREATE TABLE `ven_user` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '表id',
  `email` varchar(60) NOT NULL DEFAULT '' COMMENT '邮件',
  `password` varchar(32) NOT NULL DEFAULT '' COMMENT '密码',
  `sex` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0 保密 1 男 2 女',
  `birthday` int(11) NOT NULL DEFAULT '0' COMMENT '生日',
  `reg_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '注册时间',
  `last_login` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '最后登录时间',
  `last_ip` varchar(15) NOT NULL DEFAULT '' COMMENT '最后登录ip',
  `mobile` varchar(20) NOT NULL COMMENT '手机号码',
  `mobile_validated` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否验证手机',
  `head_pic` varchar(255) DEFAULT NULL COMMENT '头像',
  `province` int(6) DEFAULT '0' COMMENT '省份',
  `city` int(6) DEFAULT '0' COMMENT '市区',
  `district` int(6) DEFAULT '0' COMMENT '县',
  `email_validated` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否验证电子邮箱',
  `level` tinyint(1) DEFAULT '1' COMMENT '会员等级',
  `site_power` varchar(255) DEFAULT NULL COMMENT '网站权限',
  `column_power` varchar(255) DEFAULT NULL COMMENT '栏目权限',
  `article_power` text COMMENT '文章权限',
  `status` tinyint(2) DEFAULT NULL COMMENT '是否禁用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='用户表';

-- ----------------------------
-- Records of ven_user
-- ----------------------------
INSERT INTO `ven_user` VALUES ('2', '', '1cf93980abf2b3772a6772893aef8c58', '0', '0', '1513925528', '0', '', '15101574184', '1', null, '0', '0', '0', '0', '1', null, null, null, null);

-- ----------------------------
-- Table structure for ven_users
-- ----------------------------
DROP TABLE IF EXISTS `ven_users`;
CREATE TABLE `ven_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '用户名',
  `nickname` varchar(20) NOT NULL DEFAULT '' COMMENT '昵称',
  `passwd` varchar(64) NOT NULL DEFAULT '' COMMENT '密码',
  `encrypt_salt` varchar(64) NOT NULL DEFAULT '' COMMENT '加密密钥',
  `login_ip` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '最后登录ip',
  `login_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '登陆次数',
  `time_login` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最后登录时间',
  `time_create` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `time_update` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '状态，默认 0 禁用 1 启用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=502 DEFAULT CHARSET=utf8 COMMENT='后台管理员表';

-- ----------------------------
-- Records of ven_users
-- ----------------------------
INSERT INTO `ven_users` VALUES ('1', 'admins', '阿飞', '7574b81c71f76753a85fb9f93ab7dc44', 'JXYQ8dAvL3ECG5f7r9uS', '2130706433', '36', '1514510655', '1462541400', '1502787011', '1');
INSERT INTO `ven_users` VALUES ('501', 'libai', '李白', 'd31f00db118c3a3c16097ca4ad0014d1', '09be8091bfc64d60adc8e8d1e7671ce7', '167772674', '1', '1504852663', '1504852587', '1505092908', '1');

-- ----------------------------
-- Table structure for ven_user_level
-- ----------------------------
DROP TABLE IF EXISTS `ven_user_level`;
CREATE TABLE `ven_user_level` (
  `level_id` smallint(4) unsigned NOT NULL AUTO_INCREMENT COMMENT '表id',
  `level_name` varchar(30) DEFAULT NULL COMMENT '头衔名称',
  `amount` decimal(10,2) DEFAULT NULL COMMENT '等级必要金额',
  `discount` smallint(4) DEFAULT NULL COMMENT '折扣',
  `describe` varchar(200) DEFAULT NULL COMMENT '头街 描述',
  PRIMARY KEY (`level_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ven_user_level
-- ----------------------------

-- ----------------------------
-- Table structure for ven_wx_user
-- ----------------------------
DROP TABLE IF EXISTS `ven_wx_user`;
CREATE TABLE `ven_wx_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '表id',
  `uid` int(11) NOT NULL COMMENT 'uid',
  `wxname` varchar(60) NOT NULL COMMENT '公众号名称',
  `aeskey` varchar(256) NOT NULL DEFAULT '' COMMENT 'aeskey',
  `encode` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'encode',
  `appid` varchar(50) NOT NULL DEFAULT '' COMMENT 'appid',
  `appsecret` varchar(50) NOT NULL DEFAULT '' COMMENT 'appsecret',
  `wxid` varchar(64) NOT NULL COMMENT '公众号原始ID',
  `weixin` char(64) NOT NULL COMMENT '微信号',
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '类型',
  `wait_access` tinyint(1) DEFAULT '0' COMMENT '微信接入状态,0待接入1已接入',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='微信公共帐号';

-- ----------------------------
-- Records of ven_wx_user
-- ----------------------------
