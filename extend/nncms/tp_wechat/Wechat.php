<?php
/**
 * Created by PhpStorm.
 * Power By NNcms
 * Email：1600875665@qq.com
 * Date: 2017/8/30
 * Time: 9:21
 */

namespace nncms\tp_wechat;
use nncms\tp_wechat\base\ErrCode;
use nncms\tp_wechat\src\Card;
use nncms\tp_wechat\src\Custom;
use nncms\tp_wechat\src\Device;
use nncms\tp_wechat\src\Extend;
use nncms\tp_wechat\src\Hardware;
use nncms\tp_wechat\src\Media;
use nncms\tp_wechat\src\Menu;
use nncms\tp_wechat\src\Message;
use nncms\tp_wechat\src\Oauth;
use nncms\tp_wechat\src\Poi;
use nncms\tp_wechat\src\Receive;
use nncms\tp_wechat\src\Script;
use nncms\tp_wechat\src\User;
use Exception;


class Wechat
{
    static protected $instance;


    public function __construct(array $options = [])
    {
        $options = self::getOptions($options);
        $sn = md5("{$options["appid"]}{$options["appsecret"]}");
        $this->sn = $sn;

    }


    /**
     * Power: NNcms
     * Email：1600875665@qq.com
     * @param array $options
     * @return Message
     * @throws Exception
     */
    static public function message($options=[]){
        $options =self::getOptions($options);
        $sn = self::getSn($options);
        if(isset(self::$instance[$sn]["message"])){
            return self::$instance[$sn]["message"];
        }else{
            return self::$instance[$sn]["message"] = new Message($options);
        }
    }

    /**
     * Power: NNcms
     * Email：1600875665@qq.com
     * @param array $options
     * @return Device
     * @throws Exception
     */
    static public function device($options=[]){
        $options =self::getOptions($options);
        $sn = self::getSn($options);
        if(isset(self::$instance[$sn]["device"])){
            return self::$instance[$sn]["device"];
        }else{
            return self::$instance[$sn]["device"] = new Device($options);
        }
    }


    /**
     * Power: NNcms
     * Email：1600875665@qq.com
     * @param array $options
     * @return Receive
     * @throws Exception
     */
    static public function receive($options=[]){
        $options =self::getOptions($options);
        $sn = self::getSn($options);
        if(isset(self::$instance[$sn]["receive"])){
            return self::$instance[$sn]["receive"];
        }else{
            return self::$instance[$sn]["receive"] = new Receive($options);
        }
    }


    /**
     * Power: NNcms
     * Email：1600875665@qq.com
     * @param array $options
     * @return Menu
     * @throws Exception
     */
    static public function menu($options=[]){
        $options =self::getOptions($options);
        $sn = self::getSn($options);
        if(isset(self::$instance[$sn]["menu"])){
            return self::$instance[$sn]["menu"];
        }else{
            return self::$instance[$sn]["menu"] = new Menu($options);
        }
    }

    /**
     * Power: NNcms
     * Email：1600875665@qq.com
     * @param array $options
     * @return Script
     * @throws Exception
     */
    static public function script($options=[]){
        $options =self::getOptions($options);
        $sn = self::getSn($options);
        if(isset(self::$instance[$sn]["script"])){
            return self::$instance[$sn]["script"];
        }else{
            return self::$instance[$sn]["script"] = new Script($options);
        }
    }

    /**
     * Power: NNcms
     * Email：1600875665@qq.com
     * @param array $options
     * @return User
     * @throws Exception
     */
    static public function user($options=[]){
        $options =self::getOptions($options);
        $sn = self::getSn($options);
        if(isset(self::$instance[$sn]["user"])){
            return self::$instance[$sn]["user"];
        }else{
            return self::$instance[$sn]["user"] = new User($options);
        }
    }

    /**
     * Power: NNcms
     * Email：1600875665@qq.com
     * @param array $options
     * @return Card
     * @throws Exception
     */
    static public function card($options=[]){
        $options =self::getOptions($options);
        $sn = self::getSn($options);
        if(isset(self::$instance[$sn]["card"])){
            return self::$instance[$sn]["card"];
        }else{
            return self::$instance[$sn]["card"] = new Card($options);
        }
    }

    /**
     * Power: NNcms
     * Email：1600875665@qq.com
     * @param array $options
     * @return Hardware
     * @throws Exception
     */
    static public function hardware($options=[]){
        $options =self::getOptions($options);
        $sn = self::getSn($options);
        if(isset(self::$instance[$sn]["hardware"])){
            return self::$instance[$sn]["hardware"];
        }else{
            return self::$instance[$sn]["hardware"] = new Hardware($options);
        }
    }

    /**
     * Power: NNcms
     * Email：1600875665@qq.com
     * @param array $options
     * @return Extend
     * @throws Exception
     */
    static public function extend($options=[]){
        $options =self::getOptions($options);
        $sn = self::getSn($options);
        if(isset(self::$instance[$sn]["extend"])){
            return self::$instance[$sn]["extend"];
        }else{
            return self::$instance[$sn]["extend"] = new Extend($options);
        }
    }

    /**
     * Power: NNcms
     * Email：1600875665@qq.com
     * @param array $options
     * @return Custom
     * @throws Exception
     */
    static public function custom($options=[]){
        $options =self::getOptions($options);
        $sn = self::getSn($options);
        if(isset(self::$instance[$sn]["custom"])){
            return self::$instance[$sn]["custom"];
        }else{
            return self::$instance[$sn]["custom"] = new Custom($options);
        }
    }

    /**
     * Power: NNcms
     * Email：1600875665@qq.com
     * @param array $options
     * @return Oauth
     * @throws Exception
     */
    static public function oauth($options=[]){
        $options =self::getOptions($options);
        $sn = self::getSn($options);
        if(isset(self::$instance[$sn]["oauth"])){
            return self::$instance[$sn]["oauth"];
        }else{
            return self::$instance[$sn]["oauth"] = new Oauth($options);
        }
    }

    /**
     * Power: NNcms
     * Email：1600875665@qq.com
     * @param array $options
     * @return Media
     * @throws Exception
     */
    static public function media($options=[]){
        $options =self::getOptions($options);
        $sn = self::getSn($options);
        if(isset(self::$instance[$sn]["media"])){
            return self::$instance[$sn]["media"];
        }else{
            return self::$instance[$sn]["media"] = new Media($options);
        }
    }


    /**
     * Power: NNcms
     * Email：1600875665@qq.com
     * @param array $options
     * @return Poi
     * @throws Exception
     */
    static public function poi($options=[]){
        $options =self::getOptions($options);
        $sn = self::getSn($options);
        if(isset(self::$instance[$sn]["poi"])){
            return self::$instance[$sn]["poi"];
        }else{
            return self::$instance[$sn]["poi"] = new Poi($options);
        }
    }



    protected static function getSn(array $options = []){
        $options =self::getOptions($options);
        return md5("{$options["appid"]}{$options["appsecret"]}");
    }



    protected static function getOptions( $options = []){
        if (empty($options)&& !empty( Config::get("wechat.default_options_name"))){
            $options = Config::get("wechat")[Config::get("wechat.default_options_name")];
        }elseif(is_string($options)&&!empty( Config::get("wechat.$options"))){
            $options = Config::get("wechat.$options");
        }
        if (empty($options)) {
            $error[]="获取Token参数缺失";
            throw new Exception("微信配置参数不存在");
            return false ;
        }elseif(isset($options["appid"])&&isset($options["appsecret"])){
            return $options ;
        }else{
            throw new Exception("微信配置参数不完整");
            return false ;
        }
    }

    static public function getErrText($code){
        return ErrCode::getErrText($code);
    }




}