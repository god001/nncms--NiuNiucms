window.MODULE_ORDER=(function(metmod){
/*alertify.js*/
!function(){"use strict";function t(){var t={parent:document.body,version:"1.0.12",defaultOkLabel:"Ok",okLabel:"Ok",defaultCancelLabel:"Cancel",cancelLabel:"Cancel",defaultMaxLogItems:2,maxLogItems:2,promptValue:"",promptPlaceholder:"",closeLogOnClick:!1,closeLogOnClickDefault:!1,delay:2000,defaultDelay:2000,logContainerClass:"alertify-logs right",logContainerDefaultClass:"alertify-logs right",dialogs:{buttons:{holder:"<nav>{{buttons}}</nav>",ok:"<button class='ok' tabindex='1'>{{ok}}</button>",cancel:"<button class='cancel' tabindex='2'>{{cancel}}</button>"},input:"<input type='text'>",message:"<p class='msg'>{{message}}</p>",log:"<div class='{{class}}'>{{message}}</div>"},defaultDialogs:{buttons:{holder:"<nav>{{buttons}}</nav>",ok:"<button class='ok' tabindex='1'>{{ok}}</button>",cancel:"<button class='cancel' tabindex='2'>{{cancel}}</button>"},input:"<input type='text'>",message:"<p class='msg'>{{message}}</p>",log:"<div class='{{class}}'>{{message}}</div>"},build:function(t){var e=this.dialogs.buttons.ok,o="<div class='dialog'><div>"+this.dialogs.message.replace("{{message}}",t.message);return"confirm"!==t.type&&"prompt"!==t.type||(e=this.dialogs.buttons.cancel+this.dialogs.buttons.ok),"prompt"===t.type&&(o+=this.dialogs.input),o=(o+this.dialogs.buttons.holder+"</div></div>").replace("{{buttons}}",e).replace("{{ok}}",this.okLabel).replace("{{cancel}}",this.cancelLabel)},setCloseLogOnClick:function(t){this.closeLogOnClick=!!t},close:function(t,e){this.closeLogOnClick&&t.addEventListener("click",function(){o(t)}),e=e&&!isNaN(+e)?+e:this.delay,0>e?o(t):e>0&&setTimeout(function(){o(t)},e)},dialog:function(t,e,o,n){return this.setup({type:e,message:t,onOkay:o,onCancel:n})},log:function(t,e,o){var n=document.querySelectorAll(".alertify-logs > div");if(n){var i=n.length-this.maxLogItems;if(i>=0)for(var a=0,l=i+1;l>a;a++)this.close(n[a],-1)}this.notify(t,e,o)},setLogPosition:function(t){this.logContainerClass="alertify-logs "+t},setupLogContainer:function(){var t=document.querySelector(".alertify-logs"),e=this.logContainerClass;return t||(t=document.createElement("div"),t.className=e,this.parent.appendChild(t)),t.className!==e&&(t.className=e),t},notify:function(e,o,n){var i=this.setupLogContainer(),a=document.createElement("div");a.className=o||"default",t.logTemplateMethod?a.innerHTML=t.logTemplateMethod(e):a.innerHTML=e,"function"==typeof n&&a.addEventListener("click",n),i.appendChild(a),setTimeout(function(){a.className+=" show"},10),this.close(a,this.delay)},setup:function(t){function e(e){"function"!=typeof e&&(e=function(){}),i&&i.addEventListener("click",function(i){t.onOkay&&"function"==typeof t.onOkay&&(l?t.onOkay(l.value,i):t.onOkay(i)),e(l?{buttonClicked:"ok",inputValue:l.value,event:i}:{buttonClicked:"ok",event:i}),o(n)}),a&&a.addEventListener("click",function(i){t.onCancel&&"function"==typeof t.onCancel&&t.onCancel(i),e({buttonClicked:"cancel",event:i}),o(n)}),l&&l.addEventListener("keyup",function(t){13===t.which&&i.click()})}var n=document.createElement("div");n.className="alertify hide",n.innerHTML=this.build(t);var i=n.querySelector(".ok"),a=n.querySelector(".cancel"),l=n.querySelector("input"),s=n.querySelector("label");l&&("string"==typeof this.promptPlaceholder&&(s?s.textContent=this.promptPlaceholder:l.placeholder=this.promptPlaceholder),"string"==typeof this.promptValue&&(l.value=this.promptValue));var r;return"function"==typeof Promise?r=new Promise(e):e(),this.parent.appendChild(n),setTimeout(function(){n.classList.remove("hide"),l&&t.type&&"prompt"===t.type?(l.select(),l.focus()):i&&i.focus()},100),r},okBtn:function(t){return this.okLabel=t,this},setDelay:function(t){return t=t||0,this.delay=isNaN(t)?this.defaultDelay:parseInt(t,10),this},cancelBtn:function(t){return this.cancelLabel=t,this},setMaxLogItems:function(t){this.maxLogItems=parseInt(t||this.defaultMaxLogItems)},theme:function(t){switch(t.toLowerCase()){case"bootstrap":this.dialogs.buttons.ok="<button class='ok btn btn-primary' tabindex='1'>{{ok}}</button>",this.dialogs.buttons.cancel="<button class='cancel btn btn-default' tabindex='2'>{{cancel}}</button>",this.dialogs.input="<input type='text' class='form-control'>";break;case"purecss":this.dialogs.buttons.ok="<button class='ok pure-button' tabindex='1'>{{ok}}</button>",this.dialogs.buttons.cancel="<button class='cancel pure-button' tabindex='2'>{{cancel}}</button>";break;case"mdl":case"material-design-light":this.dialogs.buttons.ok="<button class='ok mdl-button mdl-js-button mdl-js-ripple-effect'  tabindex='1'>{{ok}}</button>",this.dialogs.buttons.cancel="<button class='cancel mdl-button mdl-js-button mdl-js-ripple-effect' tabindex='2'>{{cancel}}</button>",this.dialogs.input="<div class='mdl-textfield mdl-js-textfield'><input class='mdl-textfield__input'><label class='md-textfield__label'></label></div>";break;case"angular-material":this.dialogs.buttons.ok="<button class='ok md-primary md-button' tabindex='1'>{{ok}}</button>",this.dialogs.buttons.cancel="<button class='cancel md-button' tabindex='2'>{{cancel}}</button>",this.dialogs.input="<div layout='column'><md-input-container md-no-float><input type='text'></md-input-container></div>";break;case"default":default:this.dialogs.buttons.ok=this.defaultDialogs.buttons.ok,this.dialogs.buttons.cancel=this.defaultDialogs.buttons.cancel,this.dialogs.input=this.defaultDialogs.input}},reset:function(){this.parent=document.body,this.theme("default"),this.okBtn(this.defaultOkLabel),this.cancelBtn(this.defaultCancelLabel),this.setMaxLogItems(),this.promptValue="",this.promptPlaceholder="",this.delay=this.defaultDelay,this.setCloseLogOnClick(this.closeLogOnClickDefault),this.setLogPosition("bottom left"),this.logTemplateMethod=null},injectCSS:function(){if(!document.querySelector("#alertifyCSS")){var t=document.getElementsByTagName("head")[0],e=document.createElement("style");e.type="text/css",e.id="alertifyCSS",e.innerHTML=".alertify-logs>*{padding:12px 24px;color:#fff;box-shadow:0 2px 5px 0 rgba(0,0,0,.2);border-radius:1px}.alertify-logs>*,.alertify-logs>.default{background:rgba(0,0,0,.8)}.alertify-logs>.error{background:rgba(244,67,54,.8)}.alertify-logs>.success{background:rgba(76,175,80,.9)}.alertify{position:fixed;background-color:rgba(0,0,0,.3);left:0;right:0;top:0;bottom:0;width:100%;height:100%;z-index:1}.alertify.hide{opacity:0;pointer-events:none}.alertify,.alertify.show{box-sizing:border-box;transition:all .33s cubic-bezier(.25,.8,.25,1)}.alertify,.alertify *{box-sizing:border-box}.alertify .dialog{padding:12px}.alertify .alert,.alertify .dialog{width:100%;margin:0 auto;position:relative;top:50%;transform:translateY(-50%)}.alertify .alert>*,.alertify .dialog>*{width:400px;max-width:95%;margin:0 auto;text-align:center;padding:12px;background:#fff;box-shadow:0 2px 4px -1px rgba(0,0,0,.14),0 4px 5px 0 rgba(0,0,0,.098),0 1px 10px 0 rgba(0,0,0,.084)}.alertify .alert .msg,.alertify .dialog .msg{padding:12px;margin-bottom:12px;margin:0;text-align:left}.alertify .alert input:not(.form-control),.alertify .dialog input:not(.form-control){margin-bottom:15px;width:100%;font-size:100%;padding:12px}.alertify .alert input:not(.form-control):focus,.alertify .dialog input:not(.form-control):focus{outline-offset:-2px}.alertify .alert nav,.alertify .dialog nav{text-align:right}.alertify .alert nav button:not(.btn):not(.pure-button):not(.md-button):not(.mdl-button),.alertify .dialog nav button:not(.btn):not(.pure-button):not(.md-button):not(.mdl-button){background:transparent;box-sizing:border-box;color:rgba(0,0,0,.87);position:relative;outline:0;border:0;display:inline-block;-ms-flex-align:center;-ms-grid-row-align:center;align-items:center;padding:0 6px;margin:6px 8px;line-height:36px;min-height:36px;white-space:nowrap;min-width:88px;text-align:center;text-transform:uppercase;font-size:14px;text-decoration:none;cursor:pointer;border:1px solid transparent;border-radius:2px}.alertify .alert nav button:not(.btn):not(.pure-button):not(.md-button):not(.mdl-button):active,.alertify .alert nav button:not(.btn):not(.pure-button):not(.md-button):not(.mdl-button):hover,.alertify .dialog nav button:not(.btn):not(.pure-button):not(.md-button):not(.mdl-button):active,.alertify .dialog nav button:not(.btn):not(.pure-button):not(.md-button):not(.mdl-button):hover{background-color:rgba(0,0,0,.05)}.alertify .alert nav button:not(.btn):not(.pure-button):not(.md-button):not(.mdl-button):focus,.alertify .dialog nav button:not(.btn):not(.pure-button):not(.md-button):not(.mdl-button):focus{border:1px solid rgba(0,0,0,.1)}.alertify .alert nav button.btn,.alertify .dialog nav button.btn{margin:6px 4px}.alertify-logs{position:fixed;z-index:1}.alertify-logs.bottom,.alertify-logs:not(.top){bottom:16px}.alertify-logs.left,.alertify-logs:not(.right){left:16px}.alertify-logs.left>*,.alertify-logs:not(.right)>*{float:left;transform:translateZ(0);height:auto}.alertify-logs.left>.show,.alertify-logs:not(.right)>.show{left:0}.alertify-logs.left>*,.alertify-logs.left>.hide,.alertify-logs:not(.right)>*,.alertify-logs:not(.right)>.hide{left:-110%}.alertify-logs.right{right:16px}.alertify-logs.right>*{float:right;transform:translateZ(0)}.alertify-logs.right>.show{right:0;opacity:1}.alertify-logs.right>*,.alertify-logs.right>.hide{right:-110%;opacity:0}.alertify-logs.top{top:0}.alertify-logs>*{box-sizing:border-box;transition:all .4s cubic-bezier(.25,.8,.25,1);position:relative;clear:both;backface-visibility:hidden;perspective:1000;max-height:0;margin:0;padding:0;overflow:hidden;opacity:0;pointer-events:none}.alertify-logs>.show{margin-top:12px;opacity:1;max-height:1000px;padding:12px;pointer-events:auto}",t.insertBefore(e,t.firstChild)}},removeCSS:function(){var t=document.querySelector("#alertifyCSS");t&&t.parentNode&&t.parentNode.removeChild(t)}};return t.injectCSS(),{_$$alertify:t,parent:function(e){t.parent=e},reset:function(){return t.reset(),this},alert:function(e,o,n){return t.dialog(e,"alert",o,n)||this},confirm:function(e,o,n){return t.dialog(e,"confirm",o,n)||this},prompt:function(e,o,n){return t.dialog(e,"prompt",o,n)||this},log:function(e,o){return t.log(e,"default",o),this},theme:function(e){return t.theme(e),this},success:function(e,o){return t.log(e,"success",o),this},error:function(e,o){return t.log(e,"error",o),this},cancelBtn:function(e){return t.cancelBtn(e),this},okBtn:function(e){return t.okBtn(e),this},delay:function(e){return t.setDelay(e),this},placeholder:function(e){return t.promptPlaceholder=e,this},defaultValue:function(e){return t.promptValue=e,this},maxLogItems:function(e){return t.setMaxLogItems(e),this},closeLogOnClick:function(e){return t.setCloseLogOnClick(!!e),this},logPosition:function(e){return t.setLogPosition(e||""),this},setLogTemplate:function(e){return t.logTemplateMethod=e,this},clearLogs:function(){return t.setupLogContainer().innerHTML="",this},version:t.version}}var e=500,o=function(t){if(t){var o=function(){t&&t.parentNode&&t.parentNode.removeChild(t)};t.classList.remove("show"),t.classList.add("hide"),t.addEventListener("transitionend",o),setTimeout(o,e)}};if("undefined"!=typeof module&&module&&module.exports){module.exports=function(){return new t};var n=new t;for(var i in n)module.exports[i]=n[i]}else"function"==typeof define&&define.amd?define(function(){return new t}):window.alertify=new t}();
/*alertify.min.js*/
/*!
 * remark (http://getbootstrapadmin.com/remark)
 * Copyright 2016 amazingsurge
 * Licensed under the Themeforest Standard Licenses
 */
!function(global,factory){if("function"==typeof define&&define.amd)define("/Plugin/alertify",["exports","Plugin"],factory);else if("undefined"!=typeof exports)factory(exports,require("Plugin"));else{var mod={exports:{}};factory(mod.exports,global.Plugin),global.PluginAlertify=mod.exports}}(this,function(exports,_Plugin2){"use strict";Object.defineProperty(exports,"__esModule",{value:!0});var _Plugin3=babelHelpers.interopRequireDefault(_Plugin2),NAME="alertify",Alertify=function(_Plugin){function Alertify(){return babelHelpers.classCallCheck(this,Alertify),babelHelpers.possibleConstructorReturn(this,(Alertify.__proto__||Object.getPrototypeOf(Alertify)).apply(this,arguments))}return babelHelpers.inherits(Alertify,_Plugin),babelHelpers.createClass(Alertify,[{key:"getName",value:function(){return NAME}},{key:"render",value:function(){this.options.labelOk&&(this.options.okBtn=this.options.labelOk),this.options.labelCancel&&(this.options.cancelBtn=this.options.labelCancel),this.$el.data("alertifyWrapApi",this)}},{key:"show",value:function(){if("undefined"!=typeof alertify){var options=this.options;switch("undefined"!=typeof options.delay&&alertify.delay(options.delay),"undefined"!=typeof options.theme&&alertify.theme(options.theme),"undefined"!=typeof options.cancelBtn&&alertify.cancelBtn(options.cancelBtn),"undefined"!=typeof options.okBtn&&alertify.okBtn(options.okBtn),"undefined"!=typeof options.placeholder&&alertify.delay(options.placeholder),"undefined"!=typeof options.defaultValue&&alertify.delay(options.defaultValue),"undefined"!=typeof options.maxLogItems&&alertify.delay(options.maxLogItems),"undefined"!=typeof options.closeLogOnClick&&alertify.delay(options.closeLogOnClick),options.type){case"confirm":alertify.confirm(options.confirmTitle,function(){alertify.success(options.successMessage)},function(){alertify.error(options.errorMessage)});break;case"prompt":alertify.prompt(options.promptTitle,function(str,ev){var message=options.successMessage.replace("%s",str);alertify.success(message)},function(ev){alertify.error(options.errorMessage)});break;case"log":alertify.log(options.logMessage);break;case"success":alertify.success(options.successMessage);break;case"error":alertify.error(options.errorMessage);break;default:alertify.alert(options.alertMessage)}}}}],[{key:"getDefaults",value:function(){return{type:"alert",delay:5e3,theme:"bootstrap",logContainerDefaultClass:"alertify-logs right"}}},{key:"api",value:function(){return"click|show"}}]),Alertify}(_Plugin3.default);_Plugin3.default.register(NAME,Alertify),exports.default=Alertify});
/*form.js*/
$(function(){
    // 验证码输入自动转为大写
    $(document).on('change keyup','.input-codeimg',function(){
        $(this).val($(this).val().toUpperCase());
    });
    // 上传文件
    $(document).on("change keyup",".input-group-file input[type=file]",function(){
        var $self=$(this),
            $text=$(this).parents('.input-group-file').find('.form-control'),
            value="";
        if(is_lteie9) value=$(this).val();
        if(!value){
            $.each($self[0].files,function(i,file){
                if(i>0 ) value +=',';
                value +=file.name;
            });
        }
        $text.val(value);
    });
    // 验证码点击刷新
    $(document).on('click',".met-form-codeimg",function(){
        $(this).attr({src:$(this).data("src")+'&random='+Math.floor(Math.random()*9999+1)});
    });
});
// 表单验证通用
$.fn.validation=function(){
    var $self=$(this),
        self_validation=$(this).formValidation({
        locale:validation_locale,
        framework:'bootstrap4'
    });
    // 表单所处弹窗隐藏时重置验证
    $(this).parents('.modal').on('hide.bs.modal',function() {
        $self.data('formValidation').resetForm();
    });
    function success(func,afterajax_ok){
        self_validation.on('success.form.fv', function(e) {
            e.preventDefault();
            var ajax_ok=typeof afterajax_ok != "undefined" ?afterajax_ok:true;
            if(ajax_ok){
                formDataAjax(e,func);
            }else{
                $self.data('formValidation').resetForm();
                if (typeof func==="function") return func(e,$self);
            }
        })
    }
    function formDataAjax(e,func){
        var $form    = $(e.target);
        if(is_lteie9){
            $.ajax({
                url: $form.attr('action'),
                data: $form.serializeArray(),
                cache: false,
                type: 'POST',
                dataType:'json',
                success: function(result) {
                    $form.data('formValidation').resetForm();
                    if (typeof func==="function") return func(result,$form);
                }
            });
        }else{
            var formData = new FormData(),
                params   = $form.serializeArray();
            $.each(params, function(i, val) {
                formData.append(val.name, val.value);
            });
            $.ajax({
                url: $form.attr('action'),
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                dataType:'json',
                success: function(result) {
                    $form.data('formValidation').resetForm();
                    if (typeof func==="function") return func(result,$form);
                }
            });
        }
    }
    return {self_validation:self_validation,success:success,formDataAjax:formDataAjax};
}
// formValidation多语言选择
window.validation_locale='';
if("undefined" != typeof M && M['lang_pack'] && M['plugin_lang']){
    validation_locale=M['lang_pack']+'_';
    switch(M['lang_pack']){
        case 'sq':validation_locale+='AL';break;
        case 'ar':validation_locale+='MA';break;
        // case 'az':validation_locale+='az';break;
        // case 'ga':validation_locale+='ie';break;
        // case 'et':validation_locale+='ee';break;
        case 'be':validation_locale+='BE';break;
        case 'bg':validation_locale+='BG';break;
        case 'pl':validation_locale+='PL';break;
        case 'fa':validation_locale+='IR';break;
        // case 'af':validation_locale+='za';break;
        case 'da':validation_locale+='DK';break;
        case 'de':validation_locale+='DE';break;
        case 'ru':validation_locale+='RU';break;
        case 'fr':validation_locale+='FR';break;
        // case 'tl':validation_locale+='ph';break;
        case 'fi':validation_locale+='FI';break;
        // case 'ht':validation_locale+='ht';break;
        // case 'ko':validation_locale+='kr';break;
        case 'nl':validation_locale+='NL';break;
        // case 'gl':validation_locale+='es';break;
        case 'ca':validation_locale+='ES';break;
        case 'cs':validation_locale+='CZ';break;
        // case 'hr':validation_locale+='hr';break;
        // case 'la':validation_locale+='IT';break;
        // case 'lv':validation_locale+='lv';break;
        // case 'lt':validation_locale+='lt';break;
        case 'ro':validation_locale+='RO';break;
        // case 'mt':validation_locale+='mt';break;
        // case 'ms':validation_locale+='ID';break;
        // case 'mk':validation_locale+='mk';break;
        case 'no':validation_locale+='NO';break;
        case 'pt':validation_locale+='PT';break;
        case 'ja':validation_locale+='JP';break;
        case 'sv':validation_locale+='SE';break;
        case 'sr':validation_locale+='RS';break;
        case 'sk':validation_locale+='SK';break;
        // case 'sl':validation_locale+='si';break;
        // case 'sw':validation_locale+='tz';break;
        case 'th':validation_locale+='TH';break;
        // case 'cy':validation_locale+='wls';break;
        // case 'uk':validation_locale+='ua';break;
        // case 'iw':validation_locale+='';break;
        case 'el':validation_locale+='GR';break;
        case 'eu':validation_locale+='ES';break;
        case 'es':validation_locale+='ES';break;
        case 'hu':validation_locale+='HU';break;
        case 'it':validation_locale+='IT';break;
        // case 'yi':validation_locale+='de';break;
        // case 'ur':validation_locale+='pk';break;
        case 'id':validation_locale+='ID';break;
        case 'en':validation_locale+='US';break;
        case 'vi':validation_locale+='VN';break;
        case 'tc':validation_locale='zh_TW';break;
        case 'cn':validation_locale='zh_CN';break;
    }
}else{
    validation_locale='zh_CN';
}
// 表单验证初始化
if($(".met-form-validation").length) {
    window.validate=new Array();
    $(".met-form-validation").each(function(index, el) {
        validate[index]=$(el).validation();
    });
}
/*datatable.js*/
$(function(){
    var $datatable=$('[data-table-ajaxurl]');
    if($datatable.length){
        var datatable_langurl= M['navurl']+'app/system/include/static2/vendor/datatables/language/';
        // datatable多语言选择
        if("undefined" != typeof M && M['lang_pack'] && M['plugin_lang']){
            switch(M['lang_pack']){
                case 'sq':datatable_langurl+='AL';break;
                case 'ar':datatable_langurl+='MA';break;
                // case 'az':datatable_langurl+='az';break;
                // case 'ga':datatable_langurl+='ie';break;
                // case 'et':datatable_langurl+='ee';break;
                case 'be':datatable_langurl+='BE';break;
                case 'bg':datatable_langurl+='BG';break;
                case 'pl':datatable_langurl+='PL';break;
                case 'fa':datatable_langurl+='IR';break;
                // case 'af':datatable_langurl+='za';break;
                case 'da':datatable_langurl+='DK';break;
                case 'de':datatable_langurl+='DE';break;
                case 'ru':datatable_langurl+='RU';break;
                case 'fr':datatable_langurl+='FR';break;
                // case 'tl':datatable_langurl+='ph';break;
                case 'fi':datatable_langurl+='FI';break;
                // case 'ht':datatable_langurl+='ht';break;
                // case 'ko':datatable_langurl+='kr';break;
                case 'nl':datatable_langurl+='NL';break;
                // case 'gl':datatable_langurl+='es';break;
                case 'ca':datatable_langurl+='ES';break;
                case 'cs':datatable_langurl+='CZ';break;
                // case 'hr':datatable_langurl+='hr';break;
                // case 'la':datatable_langurl+='IT';break;
                // case 'lv':datatable_langurl+='lv';break;
                // case 'lt':datatable_langurl+='lt';break;
                case 'ro':datatable_langurl+='RO';break;
                // case 'mt':datatable_langurl+='mt';break;
                // case 'ms':datatable_langurl+='ID';break;
                // case 'mk':datatable_langurl+='mk';break;
                case 'no':datatable_langurl+='NO';break;
                case 'pt':datatable_langurl+='PT';break;
                case 'ja':datatable_langurl+='JP';break;
                case 'sv':datatable_langurl+='SE';break;
                case 'sr':datatable_langurl+='RS';break;
                case 'sk':datatable_langurl+='SK';break;
                // case 'sl':datatable_langurl+='si';break;
                // case 'sw':datatable_langurl+='tz';break;
                case 'th':datatable_langurl+='TH';break;
                // case 'cy':datatable_langurl+='wls';break;
                // case 'uk':datatable_langurl+='ua';break;
                // case 'iw':datatable_langurl+='';break;
                case 'el':datatable_langurl+='GR';break;
                case 'eu':datatable_langurl+='ES';break;
                case 'es':datatable_langurl+='ES';break;
                case 'hu':datatable_langurl+='HU';break;
                case 'it':datatable_langurl+='IT';break;
                // case 'yi':datatable_langurl+='de';break;
                // case 'ur':datatable_langurl+='pk';break;
                case 'id':datatable_langurl+='ID';break;
                case 'en':datatable_langurl+='English';break;
                case 'vi':datatable_langurl+='VN';break;
                case 'tc':datatable_langurl+='Chinese-traditional';break;
                default:datatable_langurl+='Chinese';break;
            }
        }else{
            datatable_langurl+='Chinese';
        }
        datatable_langurl+='.json';
        window.datatable_pagelength=$datatable.data('pagelength')||30,
        window.datatable_option={
            drawCallback: function(settings){
                if($(window).scrollTop()>$(this).offset().top) $(window).scrollTop($(this).offset().top);// 表单重绘后页面滚动回表单顶部
                if($('[data-original]',this).length) $('[data-original]',this).lazyload();
            },
            responsive: true,
            ordering: false, //是否支持排序
            searching: false, //搜索
            searchable: false, //让搜索支持ajax异步查询
            lengthChange: false,//让用户可以下拉无刷新设置显示条数
            pageLength:datatable_pagelength,//默认每一页的显示数量
            serverSide: true, //ajax服务开启
            stateSave:true,//状态保存 - 再次加载页面时还原表格状态
            language: {
                url:datatable_langurl
            },
            ajax: {
                url: $datatable.data('table-ajaxurl'),
                data: function ( v ) {
                     var l = $("input[data-table-search],select[data-table-search]"),vlist='{ ',i=0;
                     if(l.length>0){
                         l.each(function(){
                             i++;
                             var n  = '"'+$(this).attr("name")+'"',val = '"'+$(this).val()+'"';
                             if(val!='')vlist+=i==l.length?n+':'+val:n+':'+val+',';
                         });
                     }
                     vlist+=' }';
                     vlist=$.parseJSON(vlist);
                     return $.extend( {}, v, vlist );
                }
            }
        };
        if($datatable.hasClass('dataTable')) window.datatable=$datatable.DataTable(datatable_option);
    }
})
/*order.js*/
$(function(){
	// 通过锚点获取所需订单状态
    var hash=location.hash?location.hash:'#state_all';
    hash=hash.replace('#state_','');
    $('.shop-order-state a[data-state="'+hash+'"]').addClass('active');
    // 渲染订单数据
	orderList();
	// 加载更多订单、搜索订单
	$("#shop-order-more,.shop-order-keyword .input-search-btn").click(function(){
		var search=$(this).hasClass('input-search-btn')?true:false;
        orderList(search);
    })
    // 切换订单类型
    $(".shop-order-state a").click(function(){
        $('input[name="keyword"]').val('');
        setTimeout(function(){
            orderList(true);
        },0)
        location.hash='state_'+$(this).data('state');
    })
    // 确认收货
    $(document).on('click', '.btn-doreceipt', function(e) {
    	e.preventDefault();
    	var $self = $(this);
    	alertify.theme('bootstrap').okBtn(SHOPLANG.app_shop_ok).cancelBtn(SHOPLANG.app_shop_cancel).confirm(SHOPLANG.app_shop_doreceipt_if, function (ev) {
	    	$.ajax({
	    		url: $self.attr('href'),
	    		type: 'POST',
	    		dataType: 'json',
	    		data: {id:$self.data('id')},
	    		success:function(result) {
	    			if(result.error){
                        alertify.error(result.error);
                    }else if(result.success){
                    	$('input[name="keyword"]').val('');
                    	orderList(true);
                        alertify.success(result.success);
                    }
	    		}
	    	})
    	})
    });
});
// 渲染订单数据
function orderList(search){
	var $morebtn = $('#shop-order-more'),
		$order_list=$('.shop-order-list'),
		order_img_scale=$order_list.data('scale'),
		order_img_size='&x=60&y='+Math.round(60*order_img_scale),
		order_img_thumbdir=M['navurl']+'include/thumb.php?dir=';
	if(search) window.page = null;
	// $order_list.html('<div class="h-200 vertical-align text-xs-center order-loader"><div class="loader vertical-align-middle loader-default"></div></div>');
	$morebtn.attr('disabled','disabled');
	orderJson(function(json){
		if(json.success==1 && json.order.length){
			var html = window.page>1?'<hr>':'';
			$.each(json.order, function(i, item){
				html += '<div class="shop-order-lists state-'+item.state+'">'+
					'<div class="row shop-order-top">'+
						'<div class="col-md-8 col-sm-8 ting">'+
							'<h4>'+item.state_txt+'</h4>'+
							'<span class="info">'+SHOPLANG.app_shop_ordernumber+' : '+item.orderid+'</span>'+
							'<span class="info">'+item.rtime_str+'</span>'+
							'<span class="info">'+item.paytype_str+'</span>'+
						'</div>'+
						'<div class="col-md-4 col-sm-4 ting text-sm-right">'+
							SHOPLANG.app_shop_orderamount+' ：<span class="price  red-600">'+item.tprice_str+'</span>'+
						'</div>'+
					'</div>'+
					'<div class="row shop-order-bottom">'+
						'<div class="col-sm-6 col-xl-8">';
				if(item.goods_list){
					$.each(item.goods_list, function(k, val){
						val.img=order_img_thumbdir+val.img+order_img_size;
						html += '<div class="media media-xs m-t-10">'+
							'<div class="media-left">'+
								'<a href="'+val.url+'" target="_blank">'+
									'<img class="media-object" src="'+val.img+'" alt="'+val.pname+'">'+
								'</a>'+
							'</div>'+
							'<div class="media-body">'+
								'<h4 class="media-heading"><a href="'+val.url+'" target="_blank">'+val.pname+' &nbsp; '+val.para+'</a></h4>'+
								'<p>'+val.puprice_str+' x '+val.pamount+'</p>'+
							'</div>'+
						'</div>';
					})
				}
				item.btn_details_class=item.state==1 || item.state==3?'':' pull-xs-right';
				html += '</div>'+
						'<div class="col-xs-6 col-sm-3 col-xl-2 '+item.btn_details_class+'"><a href="'+item.docheck_url+'" class="btn btn-outline btn-default btn-squared m-b-0">'+SHOPLANG.app_shop_orderdetails+'</a></div>';
				if(item.state==1){
					html += '<div class="col-xs-6 col-sm-3 col-xl-2"><a href="'+payorder_url+'&id='+item.id+'" target="_blank" class="btn btn-danger btn-squared">'+SHOPLANG.app_shop_topaynow+'</a>';
				}
				if(item.state==3){
					html += '<div class="col-xs-6 col-sm-3 col-xl-2"><a href="'+doreceipt_url+'" data-id="'+item.id+'" class="btn btn-danger btn-squared btn-doreceipt">'+SHOPLANG.app_shop_doreceipt+'</a>';
				}
				html +='</div>'+
						'</div>'+
					'</div>'+
				'</div>';
			});
			$('.shop-order-list .order-loader').remove();
			if(search){
				$order_list.html(html);
			}else{
				$order_list.append(html);
			}
			$morebtn.removeAttr('disabled');
			window.page = parseInt(json.page) + 1;
			if(json.endnum<=json.page){
				$morebtn.attr({hidden:''});
			}else{
				$morebtn.removeAttr('hidden');
			}
		}else{
			// $('.shop-order-list .order-loader').remove();
			$order_list.html('<div class="h-200 vertical-align text-xs-center order-null animation-fade"><div class="vertical-align-middle font-size-18 blue-grey-500">'+SHOPLANG.app_shop_noorders+'</div></div>');
			$morebtn.attr({hidden:''});
		}
	});
}
// 获取订单数据
function orderJson(func){
	var search = '&state='+$('.shop-order-state li a.active').data('state');
	if($('input[name="keyword"]').val()!='') search+='&keyword='+$('input[name="keyword"]').val();
	if(window.page>1)search+='&page='+window.page;
	$.ajax({
		url: order_json_url,
		data: search,
		type: 'POST',
		dataType:'json',
		success: function(json) {
			func(json);
		}
	});
}
return metmod;
})(window.MODULE_ORDER||{});