$(function(){
	$('.ldc-reg-tab span').on('click', function(){
        var index = $(this).index();
        $(this).addClass('active').siblings().removeClass('active');
        $('.reg-box').eq(index).show().siblings('.reg-box').hide();
    });	
	$('input').on('focus', function(){
		$('.ldc-input').removeClass('has-error');
	});
	$("#remember,#remember1").on("change",function(){
		if(!$(this).prop("checked")){
			$(this).prop("checked",false).parent().removeClass("active");
		}else{
			$(this).prop("checked",true).parent().addClass("active");
		}
	});
	
});


//手机验证
 function blurmobile(){
     var mobile = $("#mobile").val();
     if(!(/^1[34578]\d{9}$/.test(mobile))){ 
    	 layui.use('layer', function(){ //独立版的layer无需执行这一句
          	  var $ = layui.jquery, layer = layui.layer; //独立版的layer无需执行这一句
          	  layer.alert('手机号格式错误，请稍后再试！', {icon: 5});
                })
         return false; 
     }
 }


//确认密码验证
 function blurrepwd(){
     var repwd = $("#mobile-password").val();
     var pwd = $("#mobile-confirm-password").val();
     if((repwd=='')||(pwd=='')){
    	 layui.use('layer', function(){ //独立版的layer无需执行这一句
        	  var $ = layui.jquery, layer = layui.layer; //独立版的layer无需执行这一句
        	  layer.alert('请您输入密码！', {icon: 5});
              })
              return ;
     }
     if (repwd != pwd) {  
    	 layui.use('layer', function(){ //独立版的layer无需执行这一句
         	  var $ = layui.jquery, layer = layui.layer; //独立版的layer无需执行这一句
         	  layer.alert('两次输入密码不一致，请核对！', {icon: 5});
               })
     return ;
     }
 }

 $("#get-mobile-code").click(function(){
	    var mobile =$("#mobile").val();
	    if(!mobile){
	    	 layui.use('layer', function(){ //独立版的layer无需执行这一句
	          	  var $ = layui.jquery, layer = layui.layer; //独立版的layer无需执行这一句
	          	  layer.alert('请您输入手机号，请稍后再试！', {icon: 5});
	                })
	      return ;
	    }
	    var mark = $("#get-mobile-code").attr("mark");
	    if("1"==mark){
	      settime(this);
	      $.ajax({
              url:"sendCode",    //请求的url地址
              dataType:"json",   //返回格式为json
              data:{"mobile":mobile},    //参数值
              type:"POST",   //请求方式
              success:function(res){
            	  console.log(res);
                  if (res == 1) {
                	  layui.use('layer', function(){ //独立版的layer无需执行这一句
        	          	  var $ = layui.jquery, layer = layui.layer; //独立版的layer无需执行这一句
        	          	  layer.alert('短信发送成功', {icon: 6});
        	                })
                  }else{
                	  layui.use('layer', function(){ //独立版的layer无需执行这一句
        	          	  var $ = layui.jquery, layer = layui.layer; //独立版的layer无需执行这一句
        	          	  layer.alert('服务器繁忙，请稍后再试！', {icon: 5});
        	                })
                  }
              },
          });
	    }
	  });
//短信后倒计时
 var countdown=60;
 function settime(obj) {
   if (countdown == 0) {
     $(obj).attr("disabled",false);
     $(obj).attr("mark","1");
     $(obj).html("获取验证码");
     countdown = 60;
     return;
   } else {
     $(obj).attr("disabled", true);
     $(obj).attr("mark","0");
     $(obj).html("重新发送(" + countdown + ")");
     countdown--;
   }
   setTimeout(function() {
         settime(obj) }
       ,1000)
 }
 
 
 $("#mobile-reg").click(function(){
	 
	    if(!$('#remember').is(':checked')) {
	    	layui.use('layer', function(){ //独立版的layer无需执行这一句
	          	  var $ = layui.jquery, layer = layui.layer; //独立版的layer无需执行这一句
	          	  layer.alert('请您先同意协议！否则将无法注册！', {icon: 5});
	                })
	      return ;
		}
	    var mobile =$("#mobile").val();
	    var repwd = $("#mobile-password").val();
	    var pwd = $("#mobile-confirm-password").val();
	    var code = $("#mobile-code").val();
	    if(!mobile){
	    	 layui.use('layer', function(){ //独立版的layer无需执行这一句
	          	  var $ = layui.jquery, layer = layui.layer; //独立版的layer无需执行这一句
	          	  layer.alert('请您输入手机号，请稍后再试！', {icon: 5});
	                })
	      return ;
	    }	
	    if(!code){
	    	 layui.use('layer', function(){ //独立版的layer无需执行这一句
	          	  var $ = layui.jquery, layer = layui.layer; //独立版的layer无需执行这一句
	          	  layer.alert('请您输验证码后再试！', {icon: 5});
	                })
	      return ;
	    }	
	    blurrepwd();
	      $.ajax({
           url:"register",    //请求的url地址
           dataType:"json",   //返回格式为json
           data:{"mobile":mobile,"repwd":repwd,"pwd":pwd,"code":code},    //参数值
           type:"POST",   //请求方式
           success:function(data){
        	   console.log(data);
        	   
               if (data.code == 200) {
             	  layui.use('layer', function(){ //独立版的layer无需执行这一句
     	          	  var $ = layui.jquery, layer = layui.layer; //独立版的layer无需执行这一句
     	          	  layer.alert('注册成功', {icon: 6},function(index){
     	          		  window.location.href="/index.php/member/login/guide";
     	          	});
     	                })
               }else{
             	  layui.use('layer', function(){ //独立版的layer无需执行这一句
     	          	  var $ = layui.jquery, layer = layui.layer; //独立版的layer无需执行这一句
     	          	  layer.alert(data.msg, {icon: 5});
     	                })
               }
           },
       });
	  });
 
 
 //////////////////////////////////////登录////////////////////////////////////////////
 $(function(){
	    $('#remember').on('change',function(){
			if(!$(this).prop('checked')){
				$(this).parent().removeClass('active');
			}else{
				$(this).parent().addClass('active');
			}
		});
		$('#password').on('focus', function(){
		    $('.ldc-animate').addClass('active');
		}).on('blur', function(){
			$('.ldc-animate').removeClass('active');
		});
		$('input').on('focus', function(){
			$('.ldc-input').removeClass('has-error');
		});
		});	
 
 $("#submit-login").click(function(){
	    var mobile =$("#mobile").val();
	    var pwd = $("#pwd").val();
	    if(!mobile){
	    	 layui.use('layer', function(){ //独立版的layer无需执行这一句
	          	  var $ = layui.jquery, layer = layui.layer; //独立版的layer无需执行这一句
	          	  layer.alert('请您输入手机号，请稍后再试！', {icon: 5});
	                })
	      return ;
	    }	
	    if(!pwd){
	    	 layui.use('layer', function(){ //独立版的layer无需执行这一句
	          	  var $ = layui.jquery, layer = layui.layer; //独立版的layer无需执行这一句
	          	  layer.alert('请您密码后再试！', {icon: 5});
	                })
	      return ;
	    }	
	      $.ajax({
			        url:"login",    //请求的url地址
			        dataType:"json",   //返回格式为json
			        data:{"mobile":mobile,"pwd":pwd},    //参数值
			        type:"POST",   //请求方式
			        success:function(data){
			     	   console.log(data);
			            if (data.code == 200) {
			          	  layui.use('layer', function(){ //独立版的layer无需执行这一句
			  	          	  var $ = layui.jquery, layer = layui.layer; //独立版的layer无需执行这一句
			  	          	  	layer.alert('登录成功', {icon: 6});
			  	          	  	window.location.href=data.url;
			  	           })
			            }else{
			          	  layui.use('layer', function(){ //独立版的layer无需执行这一句
			  	          	  var $ = layui.jquery, layer = layui.layer; //独立版的layer无需执行这一句
			  	          	  layer.alert(data.msg, {icon: 5});
			  	                })
			            }
			        },
			    });
	  });
 
 //////////////////////////////////////////////找回密码///////////////////////////////////////////////////
 

 $("#mobile-reg-pass").click(function(){
	    var mobile =$("#mobile").val();
	    var repwd = $("#mobile-password").val();
	    var pwd = $("#mobile-confirm-password").val();
	    var code = $("#mobile-code").val();
	    if(!mobile){
	    	 layui.use('layer', function(){ //独立版的layer无需执行这一句
	          	  var $ = layui.jquery, layer = layui.layer; //独立版的layer无需执行这一句
	          	  layer.alert('请您输入手机号，请稍后再试！', {icon: 5});
	                })
	      return ;
	    }	
	    if(!code){
	    	 layui.use('layer', function(){ //独立版的layer无需执行这一句
	          	  var $ = layui.jquery, layer = layui.layer; //独立版的layer无需执行这一句
	          	  layer.alert('请您输验证码后再试！', {icon: 5});
	                })
	      return ;
	    }	
	    blurrepwd();
	      $.ajax({
        url:"password",    //请求的url地址
        dataType:"json",   //返回格式为json
        data:{"mobile":mobile,"repwd":repwd,"pwd":pwd,"code":code},    //参数值
        type:"POST",   //请求方式
        success:function(data){
     	   console.log(data);
     	   
            if (data.code == 200) {
          	  layui.use('layer', function(){ //独立版的layer无需执行这一句
  	          	  var $ = layui.jquery, layer = layui.layer; //独立版的layer无需执行这一句
  	          	  layer.alert('修改成功', {icon: 6},function(index){
  	          		  window.location.href='/index.php/member/login/login';
  	          	});
  	                })
            }else{
          	  layui.use('layer', function(){ //独立版的layer无需执行这一句
  	          	  var $ = layui.jquery, layer = layui.layer; //独立版的layer无需执行这一句
  	          	  layer.alert(data.msg, {icon: 5});
  	                })
            }
        },
    });
	  });
