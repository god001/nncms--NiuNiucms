//banner图片切换
var timer;
function slideChange($obj){
  var index = $($obj).find('.active').index()-1;
  var next = index + 1;
  if (index == 2) {
    next = 0;
  }
  $($obj).find('.banner-box').eq(index).removeClass('active');
  $($obj).find('.banner-box').eq(next).addClass('active').find('img').css({
    'animation':'zoom 1s 1 ease-out 0.1s normal both',
    '-ms-animation':'zoom 1s 1 ease-out 0.1s normal both',
    '-moz-animation':'zoom 1s 1 ease-out 0.1s normal both',
    '-o-animation':'zoom 1s 1 ease-out 0.1s normal both',
    '-moz-animation':'zoom 1s 1 ease-out 0.1s normal both',  
  });
  timer = setTimeout("slideChange('"+$obj+"')", 3000);
}
$(function(){
    //header 
    $('#ldc-header').removeClass('ldc-header-active');
    $(window).scroll(function(){
      var scroH = $(window).scrollTop();//滚动条高度
      var flag = false;
      var ratio = scroH/30;
      $('#ldc-header').css('background','rgba(76,88,103,'+ratio+')');
      if (scroH > 30) {
          $('#ldc-header').addClass('ldc-header-active');
          $('#ldc-header').css('background','rgba(76,88,103,'+ratio+')');
      } else {
          $('#ldc-header').removeClass('ldc-header-active');
          $('#ldc-header').css('background','rgba(76,88,103,0)');
      }
    });
    
    //banner
    $('.ldc-banner-left,.ldc-banner-right').on('mouseenter', function(){
      var index = $(this).find('div.active').index()-1;
      clearTimeout(timer);
      slideChange('.'+$(this).attr('class'));
    }).on('mouseleave', function(){
      clearTimeout(timer);
    });
    //customer图标陆续出现
    var index = 0;
    var customerArr = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15];
    customerArr.sort(function(){return Math.random()>0.5?-1:1;});
    var customerInterval = window.setInterval(function(){
        if (index < customerArr.length){
            $('.ldc-customer').find('.customer-box').eq(customerArr[index]).addClass('active');
            index++;
        } else {
            clearInterval(customerInterval);
        }
    }, 100);
    $('body').css('position', 'relative');
    $('body').attr('data-spy', 'scroll');
    $('body').scrollspy({ target: '.ldc-scroll' });
    $('body').attr('data-offset', '150');
    $(window).scroll(function(){
      $.each($('.ldc-customer .customer-box'), function(i, v){
        var thisTop = $(this).offset().top;//顶部距离
        var scroH = $(window).scrollTop();//滚动条高度
        var winH = $(window).height();//可视区域高度
        var topValue = thisTop - scroH;//距离可视区域顶部高度
        if (topValue <= 194) {
          $(this).addClass('customer-bottom').removeClass('customer-top');
        } else {
          $(this).removeClass('customer-bottom').addClass('customer-top');
        }
      });
    });
    var num1 = 0;
    $('.ldc-scroll').on('activate.bs.scrollspy', function (e) {
        var id = $(this).find(".active a").attr("href").substr(1);
        if (num1 == 0) {
            $('.ldc-customer').find('.customer-box').removeClass('active');
            index = 0;
            customerArr.sort(function(){return Math.random()>0.5?-1:1;});
            var customerInterval = window.setInterval(function(){
                if (index < customerArr.length){
                    $('.ldc-customer').find('.customer-box').eq(customerArr[index]).addClass('active');
                    index++;
                } else {
                    clearInterval(customerInterval);
                }
            }, 100);
            num1 ++;
        }
    });
    
});