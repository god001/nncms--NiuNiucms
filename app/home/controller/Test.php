<?php
namespace app\home\controller;
use app\common\controller\Homebase;
use app\common\controller\WxPay;
use app\common\controller\Jssdk;
use \think\Loader;
use think\Controller;
error_reporting(0);

/*
** 前台首页
*/

class Test extends Homebase {
	
	// 优先加载
	public function  _initialize() {
		parent::_initialize();
		
		
	}
	
	// 扫码支付
	public function index()
	{
		$pay = new WxPay();
		$arr = $pay->Scancode();
		return $arr;
	}
	
	// 扫码支付
	public function notify()
	{
		$pay = new WxPay();
		$arr = $pay->notify();
		//return $arr;
	}
	//jssdk支付
	
	public function jssdkpay(){
		
		$pay = new WxPay();
		$arr = $pay->jspay();
		$appId="wx607a147f493ac596";
		$appSecret="ad25dfc7dc6b70db0339e14a4ec60d80";
		$jssdks = new Jssdk($appId, $appSecret);
		$sings = $jssdks->getSignPackage();
		$this->view->sings =$sings;
		$this->view->order =json_decode($arr,true);
		return $this->fetch('jssdk');
		
		
	}

	// 扫码支付
	public function tuikuan()
	{
		$pay = new WxPay();
		$arr = [
				'out_trade_no' => '599183461515032652',
				'total_fee' => 1,
				'refund_fee' => 1,
				'out_refund_no' => time()
		];
		$a = $pay->refund($arr);;
		return $a;
	}

}
