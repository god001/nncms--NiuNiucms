<?php
namespace app\home\controller;
use app\common\controller\Homebase;
use \think\Loader;
use app\common\model\Pay;
error_reporting(0);
// +----------------------------------------------------------------------
// | VenusCMF
// +----------------------------------------------------------------------
// | Copyright (c) 2017-2099
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 水蕃茄 <lzhf237@126.com>
// +----------------------------------------------------------------------

/*
** 前台首页
*/

class Index extends Homebase {
	
	// 优先加载
	public function  _initialize() {
		parent::_initialize();
		
	}
	
	public function index(){
		
		if (isset($_GET['code'])){
			echo $_GET['code'];
		}else{
			echo "NO CODE";
		}
		return $this->fetch();
		
	}
public function weixin()
	{//发起微信支付，得到微信支付字符串，直接输出字符串，在模板中通过jquery生成支付二维码
		if(request()->isAjax()){
			$Pay = new Pay;
			$result = $Pay->weixin([
				'body' => input('post.body/s','','trim,strip_tags'),
				'attach' => input('post.attach/s','','trim,strip_tags'),
				'out_trade_no' => input('post.orderid/s','','trim,strip_tags'),
				'total_fee' => input('post.total_fee/f',0,'trim,strip_tags')*100,//订单金额，单位为分，如果你的订单是100元那么此处应该为 100*100
				'time_start' => date("YmdHis"),//交易开始时间
				'time_expire' => date("YmdHis", time() + 604800),//一周过期
				'goods_tag' => '在线充值余额',
				'notify_url' => request()->domain().url('home/index/weixin_notify'),
				'trade_type' => 'NATIVE',
				'product_id' => rand(1,999999),
			]);
			if(!$result['code']){
				return $this->error($result['msg']);
			}
			return $this->success($result['msg']);
		}
		$this->view->orderid = date("YmdHis").rand(100000,999999);
		return $this->fetch();
	}

	public function weixin_notify()
	{//微信订单异步通知
		$result="回调方法已记录";
		Log::init([
				'type'  =>  'File',
				'path'  =>  LOG_PATH.'../paylog/'
		]);
		Log::write($result,'log');
		$notify_data = file_get_contents("php://input");//获取由微信传来的数据
		if(!$notify_data){
			$notify_data = $GLOBALS['HTTP_RAW_POST_DATA'] ?: '';//以防上面函数获取到的内容为空
		}
		if(!$notify_data){
			exit('');
		}
		$Pay = new Pay;
		$result = $Pay->notify_weixin($notify_data);//调用模型中的异步通知函数
		exit($result);
	}
	
}
