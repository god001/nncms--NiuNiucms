<?php
namespace app\admin\controller;
use app\common\controller\Adminbase;

class Datafile extends Adminbase{
	
	private  static  $file = null;//定义数据表对象
	private  static  $category = null;//定义分类数据对象
	
	/*
	 * 定义构造方法中数据对象 
	 */
	public function  _initialize(){
		
		//实例化数据对象
		self::$category = '';
		self::$file = '';
	}
}
