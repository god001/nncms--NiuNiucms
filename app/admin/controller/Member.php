<?php
namespace app\admin\controller;
use app\common\controller\Adminbase;
use \think\Loader;

/*
** 会员 控制器
*/

class Member extends Adminbase {
	private static $_user = null; 	// 数据表对象
	
	// 优先加载
	public function _initialize() {
		parent::_initialize();
        self::$_user = Loader::model('user');
	}

	public function index() {
		
			// 当前页
		$nowPage = 1;
		if(isset($_GET['page']) && $_GET['page']){
			$nowPage = input('param.page/d');
			$nowPage = $nowPage > 0 ? $nowPage : 1;
		}
		
		// 每页显示条数，默认 10 条
		$pagesize = 10;
		
		// 处理查询条件
		$where = array();
		
		// 时间
		$whereTimeS = array();
		$whereTimeE = array();
		if(isset($_GET['startime']) && $_GET['startime'] && isset($_GET['endtime']) && $_GET['endtime']){
			// 检查时间日期的合法性
			$startime = input('param.startime');
			$endtime = input('param.endtime');
			$starTimes = date('Y-m-d', strtotime($startime));
			$endTimes = date('Y-m-d', strtotime($endtime));
			if($startime === $starTimes){ }else{
				echo '<script>alert("起始时间不合法！");</script>';
				$startime = '';
			}
			if($endtime === $endTimes){ }else{
				echo '<script>alert("结束时间不合法！");</script>';
				$endtime = '';
			}
			$FindStarTime = strtotime($startime);
			$FindendTime = strtotime($endtime) + 86400 - 1;
			
			if($FindStarTime > $FindendTime){
				echo '<script>alert("开始时间不能大于结束时间！");</script>';
				$startime = '';
				$endtime = '';
			}
			// 区间查询
			if($startime && $endtime){
				$where['reg_time'] = array(
									array('egt', $FindStarTime),
									array('elt', $FindendTime),
									'and'
								);
			}
		}else{
			$startime = '';
			$endtime = '';
		}
		
		// 标题
		$mobile = '';
		if(isset($_GET['mobile']) && $_GET['mobile']){
			$mobile = input('param.mobile');
			$where['mobile'] = array('like', '%' . $mobile . '%');
		}
		
		//dump($where);
		// 获取分页后的文章列表
		$pageData = self::$_user -> userLists($where, $nowPage, $pagesize);
		
		
		$showDatas = array(
				'lists' 		=> $pageData['lists'],
				'allpages' 		=> $pageData['allpages'], 	// 总页数
				'nowpage' 		=> $pageData['nowpage'], 	// 当前页
				'startime' 		=> $startime,
				'endtime' 		=> $endtime,
				'title' 		=> $title
			);
		
		$this -> assign($showDatas);
		return view('index');
	}

	
}
