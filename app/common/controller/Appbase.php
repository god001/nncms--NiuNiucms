<?php
namespace app\common\controller;
use think\Controller;

/*
** 网站基类控制器
*/

class Appbase extends Controller {
	
	// 优先加载
	public function  _initialize() {
		
		// 版本号
		$this -> assign('version', 'ny老板1.0.1');
		// 输出系统时间
		$this -> assign('systime', time());
	}
	
}
