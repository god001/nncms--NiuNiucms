<?php
namespace app\common\controller;
use app\common\controller\Appbase;

/*
** 前台基类控制器
*/

class Homebase extends Appbase {
	
	// 优先加载
	public function  _initialize() {
		parent::_initialize();
		
	}
	
	
	/*
	 **阿里大鱼短信
	 */
	public function sendsms($mobile,$code,$sms) {
		include EXTEND_PATH.'Alidayu/TopSdk.php';
		$c = new \TopClient();
		$c->appkey = '23651270';
		$c->secretKey = 'cf4fda7be815752065e74d4e2695e296';
		$req = new \AlibabaAliqinFcSmsNumSendRequest();
		$req->setSmsType("normal");
		$req->setSmsFreeSignName("优研优选");//你在阿里大于设置的那个
		$req->setSmsParam("{'code':$code,'product':'图玛'}");//我只是用来做验证码，因此只有这一个
		$req->setRecNum($mobile);//手机号码
		$req->setSmsTemplateCode($sms);//自己的编号
		$resp = $c->execute($req);
		if($resp->result->success == true){
			return true;
		}else{
			return false;
		}
		// return $resp;//我的判断规则有专门的处理方法因此这里直接返回了，大家可以在这里做进一步判断再返回
	}
	
}
