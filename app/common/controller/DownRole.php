<?php
namespace app\common\controller;
use app\common\controller\Homebase;

/*
** 前台基类控制器
*/

class DownRole extends Appbase {
	
	// 优先加载
	public function  _initialize() {
		parent::_initialize();
		
	}
	
	/**
	 * 获取用户下载的资料属于哪个权限组
	 * 返回值：网站分类的id
	 * 传入值：登录用户id,文章id,要查询的权限属于哪个网站
	 */
	public function downsiteroles($uid,$detail){
		//查询当文章分类
		$id = db('article')->where('id',$detail)->value('cid');
		//获取当前分类的父分类
		$pid = db('category') -> where(array('id' => $id)) -> value('path');
		//￥arr为节点
		$arr = explode("-",$pid);
		return $arr['1'];
	}
	
	/**
	 * 获取用户下载的资料属于哪个分类权限组
	 * 返回值：网站分类的id
	 * 传入值：登录用户id,文章id,要查询的权限属于为栏目
	 */
	public function downcolumnroles($uid,$detail){
		//查询当文章分类
		$id = db('article')->where('id',$detail)->value('cid');
		//获取当前分类的父分类
		$pid = db('category') -> where(array('id' => $id)) -> value('path');
		//￥arr为节点
		$arr = explode("-",$pid);
		$app = array_shift($arr);
		$app = array_shift($arr);
		return $arr;
	}
	/**
	 * 将用户所有买的权限id序列化入库
	 * 返回值：ture
	 * 传入值：登录用户id,所买权限id,要查询的权限属于（1为网站，2为栏目，3为二级栏目，4为三及目录）
	 */
	public function saveroles($uid,$detail,$leaver){
		//查询当级别用户所拥有的权限
		if($leaver==1){
			$name = "site_power";
		}
		if($leaver==2){
			$name = "column_power";
		}
		if($leaver==3){
			$name = "article_power";
		}
		if($leaver==4){
			$name = "site_power";
		}
		$role = db('user') -> where(array('id' => $uid))->value($name);
		if($role==''){
			$roles[0] = $detail;
		}else{
			$roles = unserialize($role);
			$roles[] = $detail;
		}
		$lize = serialize($roles);
		$pid = db('user') -> where(array('id' => $uid))->setField($name,$lize);
		return $pid;
	}
	
	

}
