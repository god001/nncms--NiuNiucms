<?php
namespace app\common\controller;
use app\common\controller\Homebase;
use \think\Loader;
error_reporting(0);

/*
** 前台首页
*/

class WxPay extends Homebase {
	
	// 优先加载
	public function  _initialize() {
		parent::_initialize();
		
	}
	
	// 扫码支付
	public function Scancode($params)
	{
		
		$result =  \wxpay\NativePay::getPayImage($params);
		echo $result;
	}
	
	// 公众号支付
	public function jspay()
	{
		$result = \wxpay\JsapiPay::getPayParams($params);
		return $result;
		//halt($result);
	}
	
	// 小程序支付
	public function smallapp()
	{
		$params = [
				'body'         => '支付测试',
				'out_trade_no' => mt_rand().time(),
				'total_fee'    => 1,
		];
		$code = '08123gA41K4EQO1RH1B41uP2A4123gAW';
		$result = \wxpay\JsapiPay::getPayParams($params, $code);
	
		$openId = 'oCtoK0SjxW-N5qjEDgaMyummJyig';
		$result = \wxpay\JsapiPay::getParams($params, $openId);
	}
	
	// 刷卡支付
	public function micropay()
	{
		$params = [
				'body' => '支付测试',
				'out_trade_no' => mt_rand().time(),
				'total_fee' => 1,
		];
	
		$auth_code = '134628839776154108';
		$result = \wxpay\MicroPay::pay($params, $auth_code);
		halt($result);
	}
	
	// H5支付
	public function wappay()
	{

		$result = \wxpay\WapPay::getPayUrl($params);
		halt($result);
	}
	
	// 订单查询
	public function query()
	{
		$out_trade_no = '599183461515032652';
		$result = \wxpay\Query::exec($out_trade_no);
		halt($result);
	}
	
	// 退款
	public function refund($params)
	{
		
		$result = \wxpay\Refund::exec($params);
		halt($result);
	}
	
	// 退款查询
	public function refundquery()
	{
		$order_no = '290000985120170917160005';
		$result = \wxpay\RefundQuery::exec($order_no);
		halt($result);
	}
	
	// 下载对账单
	public function download()
	{
		$result = \wxpay\DownloadBill::exec('20170923');
		echo($result);
	}
	
	// 通知测试
	public function notify()
	{
		$notify = new \wxpay\Notify();
		$notify->Handle();
	}

	
}
