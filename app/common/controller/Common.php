<?php
namespace app\common\controller;
use app\common\controller\Appbase;

/*
** 后台基类控制器
*/

class Adminbase extends Appbase {
	private static $_uid 		= 0; 	// 当前登陆的管理员 id
	private static $_role_id 	= 0; 	// 当前登陆的管理员角色组 id
	
	// 优先加载
	public function  _initialize() {
		parent::_initialize();
	}
	
}
