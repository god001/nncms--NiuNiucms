<?php
namespace app\common\controller;
use think\Controller;
use think\Validate;
use think\Log;

/*
** 网站支付基类控制器
*/

class Appbase extends Controller {
	
	// 优先加载
	public function  _initialize() {
		
		// 版本号
		$this -> assign('version', 'venuscmf_tp5_V20170926');
		// 输出系统时间
		$this -> assign('systime', time());
	}
	
}
