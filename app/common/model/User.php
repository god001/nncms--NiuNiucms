<?php
namespace app\common\model;
use app\common\model\Common;
class User extends Common {
	private static $_user = null; // 数据表对象
	
	// 优先加载
	public function _initialize() {
		parent::_initialize();
		
		// 实例化数据表模型
		self::$_user = Loader::model('User');
		
	}
	

	// 获取用户分页列表
	public function userLists(
			$where 		= array(), 		// 查询条件
			$nowPage 	= 1, 			// 当前页
			$pagesize 	= 20, 			// 每页条数
			$order 		= 'reg_time desc' 	// 排序方式
	) {
		// 计算页数
		$total = $this -> where($where) -> count();
		$allPages = (int) ceil($total / $pagesize);
		// 验证输入的页码
		if($nowPage > $allPages){
			$nowPage = 1;
		}
	
		// 分页
		$pageStart = ($nowPage - 1) * $pagesize;
		$pageLimit = $pagesize;
	
		$lists = $this -> where($where)
		-> limit($pageStart, $pageLimit)
		-> order($order)
		-> select();
	
		return array(
				'lists' 	=> $lists,
				'total' 	=> $total,
				'allpages' 	=> $allPages,
				'nowpage' 	=> $nowPage
		);
	}
}
