<?php

/*
** 应用公共函数
*/


// 判断访问的客户端类型
// 本函数来源于网络
function user_agent() {
	$ua = $_SERVER['HTTP_USER_AGENT'];
	
	$iphone = strstr(strtolower($ua), 'mobile');
	$android = strstr(strtolower($ua), 'android');
	$windowsPhone = strstr(strtolower($ua), 'phone');
	
	function androidTablet($ua) {
		if(strstr(strtolower($ua), 'android') ){
			if(!strstr(strtolower($ua), 'mobile')){
				return true;
			}
		}
	}
	$androidTablet = androidTablet($ua);
	$ipad = strstr(strtolower($ua), 'ipad');
	if($androidTablet || $ipad){
		return 'tablet';
	}elseif($iphone && !$ipad || $android && !$androidTablet || $windowsPhone){
		return 'mobile';
	}else{
		return 'desktop';
	}
}

/*
** 随机字符串生成
** @param int $len 生成的字符串长度
** @return string
*/
function random_string($len = 6) {
	$chars = array(
			'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k',
			'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
			'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G',
			'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R',
			'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '0', '1', '2',
			'3', '4', '5', '6', '7', '8', '9'
		);
	
	$charsLen = count($chars) - 1;
	shuffle($chars); // 将数组打乱
	
	$output = '';
	for($i = 0; $i < $len; $i++){
		$output .= $chars[mt_rand(0, $charsLen)];
	}
	return $output;
}

// 对象转为数组
function object2array($object) {
	$result = array();
	foreach($object as $value){
		$result[] = json_decode($value, true);
	}
	return $result;
}

//判断是否为微信浏览器

function is_weixin(){

	if ( strpos($_SERVER['HTTP_USER_AGENT'],
			'MicroMessenger') !== false ) {
			return true;
	}
	return false;
}

//判断是否为手机
function isMobile(){
	$useragent=isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';
	$useragent_commentsblock=preg_match('|\(.*?\)|',$useragent,$matches)>0?$matches[0]:'';
	function CheckSubstrs($substrs,$text){
		foreach($substrs as $substr)
			if(false!==strpos($text,$substr)){
			return true;
		}
		return false;
	}
	$mobile_os_list=array('Google Wireless Transcoder','Windows CE','WindowsCE','Symbian','Android','armv6l','armv5','Mobile','CentOS','mowser','AvantGo','Opera Mobi','J2ME/MIDP','Smartphone','Go.Web','Palm','iPAQ');
	$mobile_token_list=array('Profile/MIDP','Configuration/CLDC-','160×160','176×220','240×240','240×320','320×240','UP.Browser','UP.Link','SymbianOS','PalmOS','PocketPC','SonyEricsson','Nokia','BlackBerry','Vodafone','BenQ','Novarra-Vision','Iris','NetFront','HTC_','Xda_','SAMSUNG-SGH','Wapaka','DoCoMo','iPhone','iPod');

	$found_mobile=CheckSubstrs($mobile_os_list,$useragent_commentsblock) ||
	CheckSubstrs($mobile_token_list,$useragent);

	if ($found_mobile){
		return true;
	}else{
		return false;
	}
}


/*
 查询传过来的id下面的分类以及分类下面的文章
 $cid 分类id
 $count  显示条数
 ###$type   页面类型  canale频道页   list 列表页
 */
function catchildrenarticle($cid,$count='',$type="list",$pagecount=''){
	// $GLOBALS['ArticleCatGrandson'] = array();

	// $GLOBALS['cat_id_arr'] = db('category')->getField('cat_id,parent_id');
	$son_id_arr = db('category')->where("pid", $cid)->select();

	if (!$son_id_arr) {
		// $GLOBALS['ArticleCatGrandson'] = db('category')->where("id", $cid)->find();
		// print_r($count);
		if ($type == "canale") {
			$self_id_arr = db('category')->where("id", $cid)->find();
			$art = db('article')->where("cid = {$cid}")->limit($count)->select();
			if ($art) {
				$GLOBALS['ArticleCatGrandson']['list'][$cid] = $self_id_arr;
				$GLOBALS['ArticleCatGrandson']['list'][$cid]['childarticle'] = $art;
			}
		}
		if ($type == "list") {
			$self_id_arr = db('category')->where("id", $cid)->find();
			$art = db('article')->where("cid = {$cid}")->count();
			if ($art >0) {
				$art = db('article')->where("cid = {$cid}")->paginate($pagecount);
				$page = $art->render();
				$GLOBALS['ArticleCatGrandson'][$cid] = $self_id_arr;
				$GLOBALS['ArticleCatGrandson'][$cid]['childarticle'] = $art;
				$GLOBALS['ArticleCatGrandson'][$cid]['page'] = $page;
			}
		}

	}else{
		$GLOBALS['ArticleCatGrandson']['type'] = "conale";
		foreach($son_id_arr as $k => $v){
			// $GLOBALS['ArticleCatGrandson'][$v['id']] = $v;
			catchildrenarticle($v['id'],$count,"canale",$pagecount);
		}
	}
	// print_r($GLOBALS['ArticleCatGrandson']);
	return $GLOBALS['ArticleCatGrandson'];
}


