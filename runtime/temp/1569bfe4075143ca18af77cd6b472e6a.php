<?php if (!defined('THINK_PATH')) exit(); /*a:4:{s:62:"D:\phpStudy\WWW\car\public/../app/member\view\login\login.html";i:1515647369;s:64:"D:\phpStudy\WWW\car\public/../app/member\view\common\common.html";i:1515647369;s:64:"D:\phpStudy\WWW\car\public/../app/member\view\common\header.html";i:1515647369;s:64:"D:\phpStudy\WWW\car\public/../app/member\view\common\footer.html";i:1515647369;}*/ ?>
<!DOCTYPE html>
<html lang="zh-CN">
 <head> 
  <meta charset="UTF-8" /> 
  <meta http-equiv="X-UA-Compatible" content="IE=edge" /> 
  <meta name="keywords" content="" /> 
  <meta name="description" content="" /> 
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" /> 
  <meta name="format-detection" content="telephone=no" /> 
  <link rel="stylesheet" href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" /> 
  <link rel="stylesheet" type="text/css" href="__STATIC__/member/css/animate.css" /> 
  <link rel="stylesheet" type="text/css" href="__STATIC__/member/css/style.css" /> 
  <link rel="stylesheet" href="/static/layui/css/layui.css"  media="all">
  <script type="text/javascript" src="__STATIC__/member/js/jquery-1.11.1.min.js"></script> 
  <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> 
  <script src="/static/layui/layui.js" charset="utf-8"></script>
  <script type="text/javascript">
        var _czc = _czc || [];
        _czc.push(["_setAccount", "5732305"]);
    </script> 
  <!--搜索--> 
  <link rel="stylesheet" type="text/css" href="__STATIC__/member/css/component.css" /> 
  <script src="__STATIC__/member/js/modernizr.custom.js"></script> 
  <style>   
	.sb-search.sb-search-open, .no-js .sb-search{ width:18%; float:right;}
  </style> 
  <title>登录</title> 
 </head>
  <header id="ldc-header" role="header" class="navbar navbar-default navbar-static-top navbar-fixed-top ldc-header ldc-header-active"> 
   <div class="container"> 
    <div class="navbar-header"> 
     <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#ldc-navbar" aria-expanded="false"> <span class="sr-only">Toggle navigation</span> <span class="icon icon-ldc-close"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button> 
     <a class="navbar-brand" href="index.html"><img src="__STATIC__/member/images/top_06.png" class="ldc-logo" /></a> 
    </div> 
    <nav class="collapse navbar-collapse" id="ldc-navbar"> 
     <ul class="nav navbar-nav ldc-nav-menu" style=""> 
      <li class="class=" active""=""> <a href="index.html" style="">首页</a> </li> 
      <li class=" ldc-normal-click"> <a href="javascript:;" style="">行业快讯</a></li> 
      <li class=" ldc-normal-click"> <a href="javascript:;" style="">热点分析</a> </li> 
      <li class=" ldc-normal-click"> <a href="javascript:;" style="">数据透视</a> </li> 
      <li> <a href="#" style="">专题研究</a></li> 
     </ul> 
     <ul class="nav navbar-nav navbar-right " style=""> 
      <li class="ldc-login"><a href="<?php echo url('login/login'); ?>" style="">登录</a></li> 
      <li><a href="<?php echo url('login/register'); ?>" style="">注册</a></li> 
     </ul> 
     <div id="sb-search" class="sb-search"> 
      <form> 
       <input class="sb-search-input" placeholder="搜索" type="text" value="" name="search" id="search" /> 
       <input class="sb-search-submit" type="submit" value="" /> 
       <span class="sb-icon-search"></span> 
      </form> 
     </div> 
     <script src="__STATIC__/member/js/classie.js"></script> 
     <script src="__STATIC__/member/js/uisearch.js"></script> 
     <script>
		new UISearch( document.getElementById( 'sb-search' ) );
	  </script> 
    </nav> 
   </div> 
  </header>   
    <div class="ldc-signin">
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-8 ldc-signin-container">
            <div class="ldc-signin-box">
              <div class="ldc-animate"> 
                  <img src="__STATIC__/member/images/ldc-animate1-1.png" alt="" class="animate-1 animate-1-1">
                  <img src="__STATIC__/member/images/ldc-animate1-2.png" alt="" class="animate-1 animate-1-2"> 
                  <img src="__STATIC__/member/images/ldc-animate2-1.png" alt="" class="animate-2 animate-2-1"> 
                  <img src="__STATIC__/member/images/ldc-animate2-2.png" alt="" class="animate-2 animate-2-2"> </div>
              <h1>中国商用车信息网账号登录</h1>
              <div class="ldc-input"> <i class="icon icon-ldc-account"></i>
                <input type="text" placeholder="请输入邮箱/手机号" id="mobile">
                <small class="help-block"></small> </div>
              <div class="ldc-input"> <i class="icon icon-ldc-password"></i>
                <input type="password" placeholder="请输入密码" name="password" id="pwd" maxlength="12">
                <small class="help-block"></small> </div>
              
              <div class="ldc-input ldc-remember">
                <label for="remember" class="checkbox inline pull-left"><i class="icon icon-ldc-checkbox"></i><i class="icon icon-ldc-checkbox-fill"></i>
                  <input type="checkbox" name="remember" id="remember">
                  记住密码</label>
                <a href="<?php echo url('login/password'); ?>" class="pull-right">忘记密码？</a>
                <div class="clearfix"></div>
              </div>
              <div class="ldc-input ldc-login">
                <button class="btn btn-primary" id="submit-login">登录</button>
                <small class="help-block"></small> </div>
              <div class="ldc-input ldc-reg"> <a href="<?php echo url('login/register'); ?>">马上注册一个</a> </div>
              <div class="ldc-input ldc-third">
                <h3><span></span>第三方快捷登陆<span></span> </h3>
                <div class="third-box"> <a href="#" target="_blank" title="用QQ帐号登录"><i class="icon icon-ldc-qq"></i></a> <a href="#" title="用微信帐号登录"><i class="icon icon-ldc-wechat-icon"></i></a> <a href="#" target="_blank" title="用新浪微博帐户登录"><i class="icon icon-ldc-weibo"></i></a> </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <script src="__STATIC__/member/js/login.js"></script> 
    <script type="text/javascript">
	
	
	
	</script>

	 
 <footer id="footer"> 
   <div class="container ldc-footer-info"> 
    <div class="row"> 
     <div class="col-xs-12 col-md-7"> 
      <div class="col-xs-6 col-sm-3 col-md-3"> 
       <div class="ldc-footer-item"> 
        <p>关于我们</p> 
        <ul> 
         <li><a href="#">微信运营</a></li> 
         <li><a href="#">活动策划与评估</a></li> 
        </ul> 
       </div> 
      </div> 
      <div class="col-xs-6 col-sm-3 col-md-3"> 
       <div class="ldc-footer-item"> 
        <p>广告服务</p> 
        <ul> 
         <li><a href="#">员工满意度</a></li> 
         <li><a href="#">餐饮满意度</a></li> 
        </ul> 
       </div> 
      </div> 
      <div class="col-xs-6 col-sm-3 col-md-3"> 
       <div class="ldc-footer-item"> 
        <p><b>会员注册</b></p> 
        <ul> 
         <li><a href="#">有偿收集</a></li> 
         <li><a href="#">定制研究报告</a></li> 
        </ul> 
       </div> 
      </div> 
      <div class="col-xs-6 col-sm-3 col-md-3"> 
       <div class="ldc-footer-item"> 
        <p>经销商注册</p> 
        <ul> 
         <li><a href="#">报表分析</a></li> 
         <li><a href="#">版本说明</a></li> 
        </ul> 
       </div> 
      </div> 
     </div> 
     <div class="col-xs-12 col-md-5"> 
      <div class="clearfix"></div> 
      <div class="col-sm-6 col-md-7"> 
       <div class="ldc-footer-item"> 
        <div class="customer"> 
         <span class="pull-left"> <i class="icon icon-ldc-custom-line"></i> </span> 
         <div class="pull-left"> 
          <p>客服咨询</p> 
          <span class="ldc-arial">086-10-61497119</span> 
         </div> 
        </div> 
        <div class="clearfix"></div> 
        <div class="customer cooperation"> 
         <span class="pull-left"> <i class="icon icon-ldc-cooperation"></i> </span> 
         <div class="pull-left"> 
          <p>商务咨询</p> 
          <span class="ldc-arial">086-10-61497119</span> 
         </div> 
        </div> 
        <div class="clearfix"></div> 
       </div> 
      </div> 
      <div class="col-sm-5 col-md-5" style="padding:0;"> 
       <div class="qrcode-box"> 
        <img src="__STATIC__/member/images/ldc-footer-qrcode.jpg" alt="" /> 
        <p>关注中国商用车信息网公众账号</p> 
       </div> 
      </div> 
     </div> 
    </div> 
   </div> 
   <div class="ldc-copyright">
     京ICP备09065492号 京公网安备110105010739 Copyright 2015-2020 中国商用车信息网 版权所有 
    <a href="" target="_blank">京ICP备12002572号-5</a> 
   </div>
  </footer> 
</body></html>