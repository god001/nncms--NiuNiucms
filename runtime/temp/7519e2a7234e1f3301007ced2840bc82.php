<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:62:"D:\phpStudy\WWW\car\public/../app/admin\view\weixin\index.html";i:1515651270;s:61:"D:\phpStudy\WWW\car\public/../app/admin\view\common\base.html";i:1515574682;}*/ ?>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="/static/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="/static/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="/static/admin/css/style.css?time=<?php echo $systime; ?>">
	
	
	<title><?php echo $nowmenuname; ?></title>
	
	<script>
		var root_url = "<?php echo url('index/index'); ?>";
	</script>
</head>
<body>
<div class="container-fluid topnav">
	<a href="<?php echo url('index/index'); ?>" class="brand pull-left">
		<img src="/static/admin/images/logo.png">
	</a>
	<ul class="topmenu pull-left">
		<li><a class="btn btn-info btn-launch" title="伸缩导航"><i class="fa fa-bars"></i></a></li><li><a class="btn btn-warning btn-recache" data-url="<?php echo url('setting/recache'); ?>" title="刷新缓存"><i class="fa fa-trash"></i></a></li>
		<li><a class="btn btn-success" title="网站首页"><i class="fa fa-home"></i></a></li>
	</ul>
	<ul class="userinfo pull-right">
		<li>
			<a>
				<i class="fa fa-calendar"></i> <span id="calendar"><?php echo date("Y 年 m 月 d 日", $systime); ?></span>
			</a>
		</li>
		<li><a><i class="fa fa-user"></i> <?php echo $username; ?></a></li>
		<li class="truebtn">
			<a class="truebtn btn-repasswd" data-url="<?php echo url('myinfo/repasswd'); ?>" data-title="修改密码" data-width="600" data-height="280">
				<i class="fa fa-key"></i> 修改密码
			</a>
		</li>
		<li class="truebtn">
			<a class="truebtn btn-logout" data-url="<?php echo url('publics/logout'); ?>">
				<i class="fa fa-sign-out"></i> 注销
			</a>
		</li>
	</ul>
</div>

<div class="container-fluid main">
	<div id="sidebar" class="sidebar">
		<?php echo $sidebar; ?>
	</div>
	<div class="mainbox">
		<div class="maintop">
			<div class="nowseat">
				<span>当前位置：</span>
				<span>
					<?php if($nowmenuname == 'index'){ ?>
						后台首页
					<?php }else{ ?>
						<?php echo (isset($nowmenuname) && ($nowmenuname !== '')?$nowmenuname:"后台首页"); } ?>
				</span>
			</div>
			<div class="btninfo">
				<a class="btn btn-success btn-refresh"><i class="fa fa-refresh"></i> 刷 新</a>
			</div>
		</div>
		<div class="maincont">
			
	<ul class="nav nav-tabs">
		<li  class="active"><a href="<?php echo url('weixin/index'); ?>" class="index-url">菜单列表</a></li>
		<li><a href="<?php echo url('weixin/createmenu'); ?>">新增菜单</a></li>
	</ul>
	
	<div class="box-body">
			<div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
				<div class="row">
					<div class="col-sm-6"></div>
					<div class="col-sm-6"></div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<form action="" method="post">
							<table class="table table-bordered table-hover dataTable" id="example2" role="grid" aria-describedby="example2_info">
								<thead>
								<tr role="row">
									<th style="width: 33%" class="sorting" tabindex="0" aria-controls="example2" aria-label="Rendering engine: activate to sort column ascending">菜单名称</th>
									<th style="width: 33%" class="sorting_desc" tabindex="0" aria-controls="example2" aria-label="Browser: activate to sort column ascending" aria-sort="descending">类型</th>
									<th style="width: 33%" class="sorting" tabindex="0" aria-controls="example2" aria-label="Platform(s): activate to sort column ascending">类型值</th>
								</tr>
								</thead>
								<tbody id="tbody">
									<!--父级操作-->

										<tr  role="row" class="odd  pmenu29 menu29" >
											<td>
												<input type="text" name="menu" class="topmenu" value="" placeholder="菜单名称">
												<a onclick="addcmenu(29);" class="btn btn-primary"><i class="fa fa-plus"></i></a><a onclick="delmenu(29);" class="btn btn-danger"><i class="fa fa-trash-o"></i></a></td>
											<td class="sorting_1">
												<select name="menu" style="width: 50%">
													<option selected value="view">链接</option>
													<option  value="click">触发关键字</option>
													<option  value="scancode_push">扫码</option>
													<option  value="scancode_waitmsg"> 扫码（等待信息）</option>
													<option  value="pic_sysphoto">系统拍照发图</option>
													<option  value="pic_photo_or_album">拍照或者相册发图</option>
													<option  value="pic_weixin">微信相册发图</option>
													<option  value="location_select">地理位置</option>
												</select>
											</td>
											<td>
												<input style="width: 100%" type="text" value="http://www.jiejue.cn/" name="menu[29][value]" placeholder="菜单值">
											</td>
											<input style="width: 100%" name="menu[29][pid]" type="hidden" value="0">
										</tr>
										<!--父级操作-->


																		</tbody>

							</table>
							<button class="btn btn-primary " type="button" onclick="addpmenu()">
								添加一级菜单<i class="fa fa-plus"></i>
							</button>

							<button class="btn btn-info " type="submit">
								保存
							</button>

						<input type="hidden" name="__hash__" value="a9937233638f1c034711fb2ad540c813_752de1a6dd1dd3396f36d305142cc288" /></form>
					</div>
				</div>
			</div>
		</div>

		</div>
	</div>
</div>

<div id="pop_html" style="display: none;"></div>

<script src="/static/js/jquery.min.js"></script>
<script src="/static/bootstrap/js/bootstrap.min.js"></script>
<script src="/static/admin/js/jquery.cookie.js"></script>
<script src="/static/js/layer/layer.js"></script>
<script src="/static/js/laydate/laydate.js"></script>
<script src="/static/admin/js/common.js?time=<?php echo $systime; ?>"></script>

</body>
</html>